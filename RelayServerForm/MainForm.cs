﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enyim.Caching;
using Enyim.Caching.Memcached;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace RelayServerForm
{
    public partial class MainForm : Form
    {
        private static RichTextBox rtbMsg;
        private static MemcachedClient client;

        public MainForm()
        {
            try
            {
                InitializeComponent();
                rtbMsg = richTextBox1;
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }

        public static void ShowText(String msg)
        {
            rtbMsg.AppendText(msg + "\n");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(tbAddServerIp.Text) || String.IsNullOrEmpty(tbAddCuid.Text))
                {
                    MessageBox.Show("ChatServerIP or CUID is null !");
                }
                else
                {
                    CherryChatUser ccuser = new CherryChatUser { cuid = tbAddCuid.Text, chatServerHostname = tbAddServerIp.Text };
                    if (CherryChatUserManager.AddUser(ccuser))
                    {
                        ShowText("save to memcache success.");
                    }
                    else
                    {
                        ShowText("save to memcache fail.");
                    }
                }
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                CherryChatUser ccuser = CherryChatUserManager.GetUser(tbSearchCuid.Text);
                if (ccuser != null)
                {
                    ShowText(tbSearchCuid.Text + "at [" + ccuser.chatServerHostname + "] ChatServer.");
                }
                else
                {
                    ShowText(tbSearchCuid.Text + " is not at Memcache server");
                }
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                if (CherryChatUserManager.RemoveUserByCuid(tbSearchCuid.Text))
                {
                    ShowText("Remove [" + tbSearchCuid.Text + "] success.");
                }
                else
                {
                    ShowText("Remove [" + tbSearchCuid.Text + "] fail.");
                }
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                ShowText(ServerManager.GetLowestLoadServerHostname());
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(ServerManager.GetOnlineUserCount(tbChatServerIp.Text) > 0))
                    ServerManager.AddOnlineUserCount(tbChatServerIp.Text);
                //ShowText("Add one to OnlineCount success. " + tbChatServerIp.Text + " count:" + ServerManager.GetOnlineUserCount(tbChatServerIp.Text));

                //Servers currentServers = client.Get<Servers>("OnlineCount");
                //if (currentServers != null)
                //{
                //    if (currentServers.dicOnlineCount != null)
                //    {
                //        if (currentServers.dicOnlineCount.ContainsKey(tbChatServerIp.Text))
                //            currentServers.dicOnlineCount[tbChatServerIp.Text]++;
                //        else
                //            currentServers.dicOnlineCount.Add(tbChatServerIp.Text, 0);

                //        Servers serversUpdate = new Servers { dicOnlineCount = currentServers.dicOnlineCount };
                //        bool success = client.Store(StoreMode.Replace, "OnlineCount", serversUpdate);
                //        if (success)
                //            ShowText("Add one to OnlineCount success. " + tbChatServerIp.Text + " count:" + serversUpdate.dicOnlineCount[tbChatServerIp.Text]);
                //        else
                //            ShowText("Add one OnlineCount fail.");
                //    }
                //    else
                //    {
                //        currentServers.dicOnlineCount = new Dictionary<string, int>();
                //        currentServers.dicOnlineCount.Add(tbChatServerIp.Text, 0);
                //        Servers serversUpdate = new Servers { dicOnlineCount = currentServers.dicOnlineCount };
                //        bool success = client.Store(StoreMode.Replace, "OnlineCount", serversUpdate);
                //        if (success)
                //            ShowText("Add one to OnlineCount success. " + tbChatServerIp.Text + " count:" + serversUpdate.dicOnlineCount[tbChatServerIp.Text]);
                //        else
                //            ShowText("Add one to OnlineCount fail.");
                //    }
                //}
                //else
                //{
                //    Dictionary<string, int> dicNew = new Dictionary<string, int>();
                //    dicNew.Add(tbChatServerIp.Text, 0);
                //    currentServers = new Servers { dicOnlineCount = dicNew };
                //    bool success = client.Store(StoreMode.Add, "OnlineCount", currentServers);
                //    if (success)
                //        ShowText("Add one to OnlineCount success. " + tbChatServerIp.Text + " count:" + currentServers.dicOnlineCount[tbChatServerIp.Text]);
                //    else
                //        ShowText("Add one to OnlineCount fail.");
                //}
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                ServerManager.SubOnlineUserCount(tbChatServerIp.Text);
                ShowText("Sub one to OnlineCount success. " + tbChatServerIp.Text + " count:" + ServerManager.GetOnlineUserCount(tbChatServerIp.Text));
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, int> dicOnlineUserCount = ServerManager.GetAllOnlineCount();

                int size = dicOnlineUserCount.Keys.Count;

                string[] servers = new string[size];
                dicOnlineUserCount.Keys.CopyTo(servers, 0);
                int[] count = new int[size];
                dicOnlineUserCount.Values.CopyTo(count, 0);

                for (int i = 0; i < servers.Length; i++)
                {
                    ShowText(servers[i] + ":" + count[i]);
                }
            }
            catch (Exception ex)
            {
                ShowText(ex.ToString());
            }
        }
    }
}
