﻿namespace RelayServerForm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSearchCuid = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbAddCuid = new System.Windows.Forms.TextBox();
            this.tbAddServerIp = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.tbChatServerIp = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(977, 92);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(145, 36);
            this.button7.TabIndex = 38;
            this.button7.Text = "Remove";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(566, 614);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(104, 48);
            this.button3.TabIndex = 34;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(813, 92);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(145, 36);
            this.button2.TabIndex = 33;
            this.button2.Text = "Search";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(563, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 14);
            this.label3.TabIndex = 32;
            this.label3.Text = "CUID";
            // 
            // tbSearchCuid
            // 
            this.tbSearchCuid.Location = new System.Drawing.Point(616, 100);
            this.tbSearchCuid.Name = "tbSearchCuid";
            this.tbSearchCuid.Size = new System.Drawing.Size(100, 24);
            this.tbSearchCuid.TabIndex = 31;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(977, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 36);
            this.button1.TabIndex = 30;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(563, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 29;
            this.label2.Text = "CUID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(740, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 14);
            this.label1.TabIndex = 28;
            this.label1.Text = "ChatServerIP";
            // 
            // tbAddCuid
            // 
            this.tbAddCuid.Location = new System.Drawing.Point(616, 26);
            this.tbAddCuid.Name = "tbAddCuid";
            this.tbAddCuid.Size = new System.Drawing.Size(100, 24);
            this.tbAddCuid.TabIndex = 27;
            // 
            // tbAddServerIp
            // 
            this.tbAddServerIp.Location = new System.Drawing.Point(836, 26);
            this.tbAddServerIp.Name = "tbAddServerIp";
            this.tbAddServerIp.Size = new System.Drawing.Size(100, 24);
            this.tbAddServerIp.TabIndex = 26;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(1, 1);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(536, 673);
            this.richTextBox1.TabIndex = 25;
            this.richTextBox1.Text = "";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(952, 176);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(170, 37);
            this.button6.TabIndex = 39;
            this.button6.Text = "Get Lowest Load Server";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(813, 447);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(145, 35);
            this.button8.TabIndex = 40;
            this.button8.Text = "Inti";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // tbChatServerIp
            // 
            this.tbChatServerIp.Location = new System.Drawing.Point(676, 454);
            this.tbChatServerIp.Name = "tbChatServerIp";
            this.tbChatServerIp.Size = new System.Drawing.Size(100, 24);
            this.tbChatServerIp.TabIndex = 41;
            // 
            // button9
            // 
            this.button9.Enabled = false;
            this.button9.Location = new System.Drawing.Point(977, 447);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(145, 35);
            this.button9.TabIndex = 42;
            this.button9.Text = "Sub (-1)";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(563, 457);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 14);
            this.label4.TabIndex = 43;
            this.label4.Text = "ChatServer IP";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(952, 239);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(170, 37);
            this.button10.TabIndex = 44;
            this.button10.Text = "Show all";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 676);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.tbChatServerIp);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbSearchCuid);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbAddCuid);
            this.Controls.Add(this.tbAddServerIp);
            this.Controls.Add(this.richTextBox1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbSearchCuid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAddCuid;
        private System.Windows.Forms.TextBox tbAddServerIp;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox tbChatServerIp;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button10;
    }
}