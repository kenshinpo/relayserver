﻿namespace TestLoadBalancer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbUserID = new System.Windows.Forms.TextBox();
            this.btnInformServer = new System.Windows.Forms.Button();
            this.tfInform = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(14, 13);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(431, 494);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(454, 481);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(127, 25);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(454, 41);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(127, 25);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search User";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbUserID
            // 
            this.tbUserID.Location = new System.Drawing.Point(453, 13);
            this.tbUserID.Name = "tbUserID";
            this.tbUserID.Size = new System.Drawing.Size(126, 24);
            this.tbUserID.TabIndex = 4;
            // 
            // btnInformServer
            // 
            this.btnInformServer.AccessibleName = "";
            this.btnInformServer.Location = new System.Drawing.Point(453, 100);
            this.btnInformServer.Name = "btnInformServer";
            this.btnInformServer.Size = new System.Drawing.Size(127, 25);
            this.btnInformServer.TabIndex = 5;
            this.btnInformServer.Text = "Inform ChatServer";
            this.btnInformServer.UseVisualStyleBackColor = true;
            this.btnInformServer.Click += new System.EventHandler(this.btnInformServer_Click);
            // 
            // tfInform
            // 
            this.tfInform.Location = new System.Drawing.Point(453, 72);
            this.tfInform.Name = "tfInform";
            this.tfInform.Size = new System.Drawing.Size(126, 24);
            this.tfInform.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 520);
            this.Controls.Add(this.tfInform);
            this.Controls.Add(this.btnInformServer);
            this.Controls.Add(this.tbUserID);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.richTextBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox tbUserID;
        private System.Windows.Forms.Button btnInformServer;
        private System.Windows.Forms.TextBox tfInform;
    }
}

