﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LoadBalancer;
using LoadBalancer.Connectivity;
using LoadBalancer.Service;
using LoadBalancer.Service.ForChatServerResponder;
using LoadBalancer.Service.ForWebServiceResponder;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace TestLoadBalancer
{
    public class LoadBalancerLauncher
    {
        private ServiceHost primaryhost;
        private ServiceHost adminServiceHost;
        private ServiceHost webServiceHost;

        // Normal messaging endpoint
        public IPEndPoint PrimaryEndpoint { get { return primaryhost.ListeningEndPoint; } }

        /// <summary>
        /// Start the server.
        /// </summary>
        public void Start()
        {
            try
            {
                // Listen for ChatServer
                MessageServiceFactory primaryServiceFactory = new MessageServiceFactory("Primary RS Service");
                primaryServiceFactory.RegisterResponder(new ChatServerClientDisconnectResponder());
                primaryServiceFactory.RegisterResponder(new ChatServerClientInitResponder());
                primaryServiceFactory.RegisterResponder(new ChatServerInitResponder());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForGroupPicUpdate());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForGroupSync());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForGroupTitleUpdate());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForRecv());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForRecvMedia());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForSent());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForGiftSent());
                primaryServiceFactory.RegisterResponder(new ChatServerSearchUserResponderForInviteSent());
                primaryhost = new ServiceHost(7030, primaryServiceFactory);
                primaryhost.Run();

                // Listen for GameServer
                MessageServiceFactory primaryAdminServiceFactory = new MessageServiceFactory("Admin Service");
                primaryAdminServiceFactory.RegisterResponder(new GameServerSearchUserResponderForGiftRecv());
                primaryAdminServiceFactory.RegisterResponder(new GameServerSearchUserResponderForInviteRecv());
                primaryAdminServiceFactory.RegisterResponder(new GameServerSearchUserResponderForGiftSend());
                primaryAdminServiceFactory.RegisterResponder(new GameServerSearchUserResponderForInviteSend());
                adminServiceHost = new ServiceHost(7040, primaryAdminServiceFactory);
                adminServiceHost.Run();

                // Listen for Web Service
                MessageServiceFactory webServiceFactory = new MessageServiceFactory("Web Service");
                webServiceFactory.RegisterResponder(new WSSearchUserResponderForLogoutChatAccount());
                webServiceFactory.RegisterResponder(new WSSearchUserResponderForLogoutCherryAccount());
                webServiceFactory.RegisterResponder(new WSSearchUserResponderForAddFriend());
                webServiceFactory.RegisterResponder(new WSSearchUserResponderForUpdateProfile());
                webServiceHost = new ServiceHost(7050, webServiceFactory);
                webServiceHost.Run();

            }
            catch (Exception e)
            {
                Form1.ShowText("Start failed: " + e.Message.ToString());
            }

        }

        public void SearchUser(string chatUserID)
        {
            CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserID);
            if (ccuser != null)
            {
                Form1.ShowText(chatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer.");
            }
            else
            {
                Form1.ShowText(chatUserID + " is not at Memcache server");
            }
        }

        public void InformChatServer(string ipAddress)
        {
            Form1.ShowText("Informing intended ChatServer....");
            primaryhost.sendMessageToIntendedChatServer(ipAddress, "Input message here");
        }

        /// <summary>
        /// Close the server.
        /// </summary>
        public void Close()
        {
            primaryhost.Stop();
            webServiceHost.Stop();
            adminServiceHost.Stop();
        }
    }
}
