﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CherryChat.Connectivity.PushNotification;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;
using TestLoadBalancer;

namespace LoadBalancer.Service.ForChatServerResponder
{
    public class ChatServerSearchUserResponderForRecvMedia : MessageResponder
    {
        public ChatServerSearchUserResponderForRecvMedia()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_SEARCHUSERFORRECVMEDIA.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Message for sending: " + message);

                ChatServerSearchUserMessageForRecvMedia msg = RealizeMessage<ChatServerSearchUserMessageForRecvMedia>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(msg.ChatUserID);
                if (ccuser != null)
                {
                    Form1.ShowText(msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    // Ask intended chat server to send message
                    ServerSession chatServerToBeDispatched = onlineChatServers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@RECVMEDIA>" + msg.ChatID + "<@CHATID>" + msg.MsgID + "<@MSGID>" + msg.SenderChatUserID + "<@SENDERCHATUSERID>" + msg.Message + "<@MESSAGE>" + msg.Type + "<@TYPE>" + msg.Timestamp + "<@TIMESTAMP>" + "<@EOF>";
                        Form1.ShowText("Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
                else
                {
                    Form1.ShowText(msg.ChatUserID + " is not at Memcache server. (OFFLINE)");

                    // Send notification 
                    Form1.ShowText("Push notification");

                    if (!string.IsNullOrEmpty(msg.NotificationMessage))
                    {
                        if (msg.NotificationMessage.Length > 100) msg.NotificationMessage = msg.NotificationMessage.Substring(0, 97) + "...";
                        APNSManager.getInstance().SendNotification(msg.DeviceToken, msg.NotificationMessage, msg.ChatID, msg.Badge, msg.OperatingType);
                    }
                }
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
