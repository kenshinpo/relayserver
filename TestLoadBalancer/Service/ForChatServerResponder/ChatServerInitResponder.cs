﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using LoadBalancer;
using TestLoadBalancer;
using RelayServerCore.LoadBalancing;

namespace LoadBalancer.Service.ForChatServerResponder
{
    // Establish connection between ChatServer and RelayServer
    public class ChatServerInitResponder : MessageResponder
    {
        public ChatServerInitResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_INIT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("ChatServer init: " + message);
                ChatServerInitMessage msg = RealizeMessage<ChatServerInitMessage>(message);

                currentServer.ipAddress = msg.ipAddress;

                // Create if not exist
                var existingChatServer = onlineChatServers[msg.ipAddress];

                if (existingChatServer == null)
                {
                    onlineChatServers[msg.ipAddress] = currentServer;
                    /* write to memcached */
                    ServerManager.InitChatServer(currentServer.ipAddress);
                }

                ChatServerInitAckMessage ack = new ChatServerInitAckMessage() { status = "true" };
                ReplyMessage(currentServer, ack);
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
