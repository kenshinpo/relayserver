﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CherryChat.Connectivity.PushNotification;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;
using TestLoadBalancer;

namespace LoadBalancer.Service.ForChatServerResponder
{
    public class ChatServerSearchUserResponderForInviteSent : MessageResponder
    {
        public ChatServerSearchUserResponderForInviteSent()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_SEARCHUSERFORINVITESENT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Message for invite sent: " + message);

                ChatServerSearchUserMessageForInviteSent msg = RealizeMessage<ChatServerSearchUserMessageForInviteSent>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(msg.ChatUserID);
                if (ccuser != null)
                {
                    Form1.ShowText(msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    // Ask intended chat server to send message
                    ServerSession chatServerToBeDispatched = onlineChatServers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@SENTINVITE>" + msg.ChatID + "<@CHATID>" + msg.MsgID + "<@MSGID>" + "<@EOF>";
                        Form1.ShowText("Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
