﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using LoadBalancer;
using TestLoadBalancer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace LoadBalancer.Service.ForChatServerResponder
{
    // Establish connection between Client-ChatServer and RelayServer
    public class ChatServerClientDisconnectResponder : MessageResponder
    {
        public ChatServerClientDisconnectResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.CLIENT_CHATSERVER_DISCONNECT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Client-ChatServer disconnect: " + message);
                ClientChatServerDisconnectMessage msg = RealizeMessage<ClientChatServerDisconnectMessage>(message);

                if (CherryChatUserManager.RemoveUserByCuid(msg.ChatUserID))
                {
                    Form1.ShowText("Remove [" + msg.ChatUserID + "] success.");
                }
                else
                {
                    Form1.ShowText("Remove [" + msg.ChatUserID + "] fail.");
                }
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
