﻿
using System;
using System.Linq;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using TestLoadBalancer;
using RelayServerCore.LoadBalancing;
using RelayServerCore.Data.Entity;

namespace LoadBalancer.Service.ForChatServerResponder
{
    // Establish connection between Client-ChatServer and RelayServer
    public class ChatServerClientInitResponder : MessageResponder
    {
        public ChatServerClientInitResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.CLIENT_CHATSERVER_INIT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Client-ChatServer init: " + message);
                ClientChatServerInitMessage msg = RealizeMessage<ClientChatServerInitMessage>(message);

                CherryChatUser ccuser = new CherryChatUser { cuid = msg.ChatUserID, chatServerHostname = currentServer.ipAddress };
                if (CherryChatUserManager.AddUser(ccuser))
                {
                    Form1.ShowText(String.Format("User {0} was saved to memcache", msg.ChatUserID));
                }
                else
                {
                    Form1.ShowText(String.Format("User {0} was not saved to memcache", msg.ChatUserID));
                }
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
