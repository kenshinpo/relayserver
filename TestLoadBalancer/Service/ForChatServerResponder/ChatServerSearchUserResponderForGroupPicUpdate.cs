﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using LoadBalancer;
using TestLoadBalancer;
using RelayServerCore.LoadBalancing;
using RelayServerCore.Data.Entity;

namespace LoadBalancer.Service.ForChatServerResponder
{
    public class ChatServerSearchUserResponderForGroupPicUpdate : MessageResponder
    {
        public ChatServerSearchUserResponderForGroupPicUpdate()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_SEARCHUSERFORGROUPPICUPDATE.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Message for group pic update: " + message);

                ChatServerSearchUserMessageForGroupPicUpdate msg = RealizeMessage<ChatServerSearchUserMessageForGroupPicUpdate>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                    Form1.ShowText(msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    // Ask intended chat server to send message
                    ServerSession chatServerToBeDispatched = onlineChatServers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@GROUPUPDATEPICSYNC>" + msg.ChatID + "<@CHATID>" + msg.GroupPicURL + "<@GROUPPICURL>" + msg.Message + "<@MESSAGE>" + msg.Timestamp + "<@TIMESTAMP>" + "<@EOF>";
                        Form1.ShowText("Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
               
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
