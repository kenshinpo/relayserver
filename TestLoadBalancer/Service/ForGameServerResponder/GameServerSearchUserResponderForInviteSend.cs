﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CherryChat.Connectivity.PushNotification;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;
using TestLoadBalancer;

namespace LoadBalancer.Service.ForChatServerResponder
{
    public class GameServerSearchUserResponderForInviteSend : MessageResponder
    {
        public GameServerSearchUserResponderForInviteSend()
            : base()
        {
            MessageOfInterested = MessageFormat.GAMESERVER_SEARCHUSERFORINVITESEND.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Message for invite send: " + message);

                GameServerSearchUserMessageForInviteSend msg = RealizeMessage<GameServerSearchUserMessageForInviteSend>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(msg.ChatUserID);
                if (ccuser != null)
                {
                    Form1.ShowText(msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@SENDINVITE>" + msg.ChatID + "<@CHATID>" + msg.MsgID + "<@MSGID>" + msg.RecvChatUserID + "<@RECVCHATUSERID>" + msg.Message + "<@MESSAGE>" + msg.Type + "<@TYPE>" + msg.Timestamp + "<@TIMESTAMP>" + "<@EOF>";
                        Form1.ShowText("Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
               
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
