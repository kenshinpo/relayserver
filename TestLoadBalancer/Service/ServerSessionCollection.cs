﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoadBalancer.Service;

namespace LoadBalancer.Service
{
    /// <summary>
    /// Encapsulates a collection of ServerSession object. 
    /// The purpose of this class is to reduce the change to other code if we changed the implementation of ServerSession storage.
    /// </summary>
    public class ServerSessionCollection
    {
        private SortedDictionary<string, ServerSession> sessions;

        public ServerSessionCollection()
        {
            sessions = new SortedDictionary<string, ServerSession>();
        }

        public ServerSession this[string ipAddress]
        {
            get
            {
                if (Contains(ipAddress)) return sessions[ipAddress];
                return null;
            }

            set
            {
                sessions[ipAddress] = value;
            }
        }

        public void Add(string ipAddress, ServerSession transport)
        {
            sessions.Add(ipAddress, transport);
        }

        public void Remove(string ipAddress)
        {
            sessions.Remove(ipAddress);
        }

        public bool Contains(string ipAddress)
        {
            return sessions.ContainsKey(ipAddress);
        }

        public int Count()
        {
            return sessions.Count;
        }

        public void ForEach(Action<ServerSession> action)
        {
            var keys = sessions.Keys.ToArray();
            foreach (var s in keys) action(sessions[s]);
        }
    }
}
