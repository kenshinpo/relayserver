﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using LoadBalancer;
using TestLoadBalancer;
using RelayServerCore.LoadBalancing;
using RelayServerCore.Data.Entity;

namespace LoadBalancer.Service.ForWebServiceResponder
{
    public class WSSearchUserResponderForAddFriend : MessageResponder
    {
        public WSSearchUserResponderForAddFriend()
            : base()
        {
            MessageOfInterested = MessageFormat.WS_SEARCHUSERFORADDFRIEND.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Message for add friend: " + message);

                WSSearchUserMessageForAddFriend msg = RealizeMessage<WSSearchUserMessageForAddFriend>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                    Form1.ShowText(msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@ADDFRIEND>" + msg.FriendChatUserID + "<@FRIENDCHATUSERID>" + msg.ChatUserID + "<@CHATUSERID>" + msg.ProfileName + "<@PROFILENAME>" +
                            msg.ProfileImageURL + "<@PROFILEIMAGEURL>" + msg.Status + "<@STATUS>" + msg.LastUpdatedProfileDate + "<@LASTUPDATEDPROFILEDATE>" + msg.RelationshipType + "<@RELATIONSHIPTYPE>" + "<@EOF>";
                        Form1.ShowText("Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
               
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
