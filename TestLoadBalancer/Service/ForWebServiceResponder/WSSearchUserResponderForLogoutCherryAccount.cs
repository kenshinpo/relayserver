﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using LoadBalancer;
using TestLoadBalancer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;
using CherryChat.Connectivity.PushNotification;

namespace LoadBalancer.Service.ForWebServiceResponder
{
    public class WSSearchUserResponderForLogoutCherryAccount : MessageResponder
    {
        public WSSearchUserResponderForLogoutCherryAccount()
            : base()
        {
            MessageOfInterested = MessageFormat.WS_SEARCHUSERFORLOGOUTCHERRYACCOUNT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Message for logout cherry account: " + message);

                WSSearchUserMessageForLogoutCherryAccount msg = RealizeMessage<WSSearchUserMessageForLogoutCherryAccount>(message);
                string chatUserIDToBeSearched = msg.ExistingChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                    Form1.ShowText(msg.ExistingChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@LOGOUTCHERRYACCOUNT>" + msg.ExistingChatUserID + "<@EXISTINGCHATUSERID>" + "<@EOF>";
                        Form1.ShowText("Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
                else
                {
                    Form1.ShowText(msg.ExistingChatUserID + " is not at Memcache server. (OFFLINE)");

                    // Send notification 
                    Form1.ShowText("Push notification");

                    if (msg.NotificationMessage.Length > 100) msg.NotificationMessage = msg.NotificationMessage.Substring(0, 97) + "...";
                    APNSManager.getInstance().SendNotification(msg.DeviceToken, msg.NotificationMessage, "0", 0, msg.OperatingType);
                }
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
