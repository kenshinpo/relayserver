﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadBalancer.Message;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using LoadBalancer;
using TestLoadBalancer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace LoadBalancer.Service.ForWebServiceResponder
{
    public class WSSearchUserResponderForUpdateProfile : MessageResponder
    {
        public WSSearchUserResponderForUpdateProfile()
            : base()
        {
            MessageOfInterested = MessageFormat.WS_SEARCHUSERFORPROFILEUPDATE.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                Form1.ShowText("Message for profile update: " + message);

                WSSearchUserMessageForUpdateProfile msg = RealizeMessage<WSSearchUserMessageForUpdateProfile>(message);
                string chatUserIDToBeSearched = msg.FriendChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                    Form1.ShowText(msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers =  MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@UPDATEPROFILE>" + msg.ChatUserID + "<@CHATUSERID>" + msg.ProfileName + "<@PROFILENAME>" + msg.ProfileImageURL + "<@PROFILEIMAGEURL>" + msg.Status + "<@STATUS>"
                            + msg.LastUpdatedProfileDate + "<@LASTUPDATEDPROFILEDATE>" + "<@EOF>";
                        Form1.ShowText("Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
            }
            catch (Exception ex)
            {
                Form1.ShowText(ex.ToString());
            }
        }
    }
}
