﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestLoadBalancer
{
    public partial class Form1 : Form
    {
        private static RichTextBox rtbMsg;
        private LoadBalancerLauncher lbl;

        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;

            rtbMsg = richTextBox1;
            lbl = new LoadBalancerLauncher();
            lbl.Start();
        }

        public static void ShowText(String msg)
        {
            rtbMsg.AppendText(msg + "\n");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            lbl.SearchUser(tbUserID.Text.ToString());
        }

        private void btnInformServer_Click(object sender, EventArgs e)
        {
            lbl.InformChatServer(tfInform.Text.ToString());
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            lbl.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            rtbMsg.Text = "";
        }
    }
}
