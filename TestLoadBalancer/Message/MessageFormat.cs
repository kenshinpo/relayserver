﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message
{
    /**
     *  The schema for all kind of messages.
     */
    static class MessageFormat
    {
        #region For ChatServer
        //!< ChatServer sends this message to CS when it is logging in.
        public static string[] CHATSERVER_INIT = { "<@CHATSERVER_INIT>", "<@IP>", "<@EOF>" };
        //!< CS replies this message to ChatServer upon success log in.
        public static string[] CHATSERVER_INITACK = { "<@CHATSERVER_INITACK>", "<@STATUS>", "<@EOF>" };
        //!< ChatServer sends this message to CS when it is logging in.
        public static string[] CLIENT_CHATSERVER_INIT = { "<@CLIENT_CHATSERVER_INIT>", "<@CHATUSERID>", "<@EOF>" };
        //!< ChatServer sends this message to CS when it is logging in.
        public static string[] CLIENT_CHATSERVER_DISCONNECT = { "<@CLIENT_CHATSERVER_DISCONNECT>", "<@CHATUSERID>", "<@EOF>" };
        //!< ChatServer sends this message to CS upon client search. 
        public static string[] CHATSERVER_SEARCHUSERFORRECV = { "<@CHATSERVER_SEARCHUSERFORRECV>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@MESSAGE>", "<@TYPE>", "<@TIMESTAMP>", "<@NOTIFICATIONMESSAGE>", "<@BADGE>", "<@OPERATINGTYPE>", "<@EOF>" };
        //!< ChatServer sends this media to CS upon client search. 
        public static string[] CHATSERVER_SEARCHUSERFORRECVMEDIA = { "<@CHATSERVER_SEARCHUSERFORRECVMEDIA>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@MESSAGE>", "<@TYPE>", "<@TIMESTAMP>", "<@NOTIFICATIONMESSAGE>", "<@BADGE>", "<@OPERATINGTYPE>", "<@EOF>" };
        //!< ChatServer sends this SENT message to CS upon client search for media and normal message.
        public static string[] CHATSERVER_SEARCHUSERFORSENT = { "<@CHATSERVER_SEARCHUSERFORSENT>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        //!< ChatServer sends this message for group sync to CS upon client search.
        public static string[] CHATSERVER_SEARCHUSERFORGROUPSYNC = { "<@CHATSERVER_SEARCHUSERFORGROUPSYNC>", "<@CHATUSERID>", "<@CHATID>", "<@TITLE>", "<@GROUPPICURL>", "<@ADMINID>", "<@MEMBERSID>", "<@MEMBERSPROFILENAME>", "<@TIMESTAMP>", "<@EOF>" };
        //!< ChatServer sends this message for group pic update to CS upon client search.
        public static string[] CHATSERVER_SEARCHUSERFORGROUPPICUPDATE = { "<@CHATSERVER_SEARCHUSERFORGROUPPICUPDATE>", "<@CHATUSERID>", "<@CHATID>", "<@GROUPPICURL>", "<@MESSAGE>", "<@TIMESTAMP>", "<@EOF>" };
        //!< ChatServer sends this message for group title to CS upon client search.
        public static string[] CHATSERVER_SEARCHUSERFORGROUPTITLEUPDATE = { "<@CHATSERVER_SEARCHUSERFORGROUPTITLEUPDATE>", "<@CHATUSERID>", "<@CHATID>", "<@TITLE>", "<@MESSAGE>", "<@TIMESTAMP>", "<@EOF>" };
        //!< ChatServer sends this gift sent to CS upon client search. 
        public static string[] CHATSERVER_SEARCHUSERFORGIFTSENT = { "<@CHATSERVER_SEARCHUSERFORGIFTSENT>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        //!< ChatServer sends this invite sent to CS upon client search. 
        public static string[] CHATSERVER_SEARCHUSERFORINVITESENT = { "<@CHATSERVER_SEARCHUSERFORINVITESENT>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        #endregion

        #region For GameServer
        //!< GameServer sends this gift recv message to CS upon client search. 
        public static string[] GAMESERVER_SEARCHUSERFORGIFTRECV = { "<@GAMESERVER_SEARCHUSERFORGIFTRECV>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@MESSAGE>", "<@TYPE>", "<@TIMESTAMP>", "<@NOTIFICATIONMESSAGE>", "<@BADGE>", "<@OPERATINGTYPE>", "<@EOF>" };
        //!< GameServer sends this invite recv to CS upon client search. 
        public static string[] GAMESERVER_SEARCHUSERFORINVITERECV = { "<@GAMESERVER_SEARCHUSERFORINVITERECV>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@MESSAGE>", "<@TYPE>", "<@TIMESTAMP>", "<@NOTIFICATIONMESSAGE>", "<@BADGE>", "<@OPERATINGTYPE>", "<@EOF>" };
        //!< GameServer sends this gift send message to CS upon client search. 
        public static string[] GAMESERVER_SEARCHUSERFORGIFTSEND = { "<@GAMESERVER_SEARCHUSERFORGIFTSEND>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@MESSAGE>", "<@TYPE>", "<@TIMESTAMP>", "<@EOF>" };
        //!< GameServer sends this invite send to CS upon client search. 
        public static string[] GAMESERVER_SEARCHUSERFORINVITESEND = { "<@GAMESERVER_SEARCHUSERFORINVITESEND>", "<@CHATUSERID>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@MESSAGE>", "<@TYPE>", "<@TIMESTAMP>", "<@EOF>" };
        #endregion

        #region For Web Service
        //!< WS sends this message for logout cherry account to CS upon client search.
        public static string[] WS_SEARCHUSERFORLOGOUTCHERRYACCOUNT = { "<@WS_SEARCHUSERFORLOGOUTCHERRYACCOUNT>", "<@EXISTINGCHATUSERID>", "<@EOF>" };
        //!< WS sends this message for logout chat account to CS upon client search.
        public static string[] WS_SEARCHUSERFORLOGOUTCHATACCOUNT = { "<@WS_SEARCHUSERFORLOGOUTCHATACCOUNT>", "<@EXISTINGCHATUSERID>", "<@EOF>" };
        //!< WS sends this message for profile update to CS upon client search.
        public static string[] WS_SEARCHUSERFORPROFILEUPDATE = { "<@WS_SEARCHUSERFORPROFILEUPDATE>", "<@FRIENDCHATUSERID>", "<@CHATUSERID>", "<@PROFILENAME>", "<@PROFILEIMAGEURL>", "<@STATUS>", "<@LASTUPDATEDPROFILEDATE>", "<@EOF>" };
        //!< WS sends this message for add friend to CS upon client search.
        public static string[] WS_SEARCHUSERFORADDFRIEND = { "<@WS_SEARCHUSERFORADDFRIEND>", "<@FRIENDCHATUSERID>", "<@CHATUSERID>", "<@PROFILENAME>", "<@PROFILEIMAGEURL>", "<@STATUS>", "<@LASTUPDATEDPROFILEDATE>", "<@RELATIONSHIPTYPE>", "<@EOF>" };
        #endregion

    }
}
