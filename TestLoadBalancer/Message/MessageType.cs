﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace LoadBalancer.Message
{
    /// <summary>
    /// Static helper class which provides StringBuilder type information.
    /// </summary>
    static class StringBuilderType
    {
        private static Type stringBuilder = typeof(StringBuilder);
        private static Dictionary<Type, MethodInfo> cache = new Dictionary<Type, MethodInfo>(10);
        private static MethodInfo toStr = null;

        /// <summary>
        /// Get StringBuilder.Append MethodInfo for various overload.
        /// </summary>
        /// <param name="type">The type of StringBuilder.Append parameter.</param>
        /// <returns>StringBuilder.Append MethodInfo</returns>
        public static MethodInfo GetAppendInfo(Type type)
        {
            if (cache.ContainsKey(type)) return cache[type];
            cache[type] = stringBuilder.GetMethod("Append", new Type[] { type }, null);
            return cache[type];
        }

        /// <summary>
        /// Get StringBuilder.ToString MethodInfo.
        /// </summary>
        /// <returns>StringBuilder.ToString MethodInfo</returns>
        public static MethodInfo GetToStringInfo()
        {
            if (toStr != null) return toStr;
            toStr = stringBuilder.GetMethod("ToString", new Type[] { }, null);
            return toStr;
        }

        /// <summary>
        /// Clear the cached type information to free up memory.
        /// </summary>
        public static void ClearInfoCache()
        {
            cache = new Dictionary<Type, MethodInfo>(10);
            toStr = null;
        }
    }

    /// <summary>
    /// Static helper class which provides conversation related type information.
    /// </summary>
    static class ConversionType
    {
        private static Dictionary<Type, MethodInfo> cache = new Dictionary<Type, MethodInfo>(10);
        private static MethodInfo intArrayInfo = typeof(ArrayConversionHelper).GetMethod("ParseInt32Array");
        private static MethodInfo longArrayInfo = typeof(ArrayConversionHelper).GetMethod("ParseInt64Array");
        private static MethodInfo intArrayJoin = typeof(ArrayConversionHelper).GetMethod("Int32ArrayToString");
        private static MethodInfo longArrayJoin = typeof(ArrayConversionHelper).GetMethod("Int64ArrayToString");

        /// <summary>
        /// Get string parsing method information for given type.
        /// </summary>
        /// <param name="type">The target type of parsing.</param>
        /// <returns>The correspond MethodInfo.</returns>
        public static MethodInfo GetParseInfo(Type type)
        {
            if (cache.ContainsKey(type)) return cache[type];
            cache[type] = type.GetMethod("Parse", new Type[] { typeof(string) }, null);
            return cache[type];
        }

        /// <summary>
        /// Get the join array method information for given type.
        /// </summary>
        /// <param name="type">The type of array to be joined.</param>
        /// <returns>The correspond MethodInfo.</returns>
        public static MethodInfo GetJoinArrayInfo(Type type)
        {
            if (type == typeof(int[])) return intArrayJoin;
            if (type == typeof(long[])) return longArrayJoin;
            throw new NotImplementedException("Only array of int and long type are supported.");
        }

        /// <summary>
        /// Get the method information of method which parse string into array.
        /// </summary>
        /// <param name="type">The type of array to be parsed.</param>
        /// <returns>The correspond MethodInfo.</returns>
        public static MethodInfo GetArrayParseInfo(Type type)
        {
            if (type == typeof(int[])) return intArrayInfo;
            if (type == typeof(long[])) return longArrayInfo;
            throw new NotImplementedException("Only array of int and long type are supported.");
        }

        /// <summary>
        /// Clear the cached type information to free up memory.
        /// </summary>
        public static void ClearInfoCache()
        {
            cache = new Dictionary<Type, MethodInfo>(10);
        }
    }

    /// <summary>
    /// Static helper class which provides array operation related type information. 
    /// </summary>
    static class ArrayConversionHelper
    {
        public static string Int32ArrayToString(int[] arr)
        {
            return String.Join(",", Array.ConvertAll<int, string>(arr, Convert.ToString));
        }

        public static string Int64ArrayToString(long[] arr)
        {
            return String.Join(",", Array.ConvertAll<long, string>(arr, Convert.ToString));
        }

        public static int[] ParseInt32Array(string str)
        {
            int index = 0;
            string[] array = str.Split(',');
            int[] result = new int[array.Length];
            foreach (string s in array) result[index++] = Int32.Parse(s);
            return result;
        }

        public static long[] ParseInt64Array(string str)
        {
            int index = 0;
            string[] array = str.Split(',');
            long[] result = new long[array.Length];
            foreach (string s in array) result[index++] = Int64.Parse(s);
            return result;
        }
    }

    /// <summary>
    /// The base class of all message type. 
    /// Provide the infrastructure of runtime code generation to make custom ToString() and Realize() with least code written.
    /// </summary>
    /// <typeparam name="VariantType">The subclass of this type.</typeparam>
    public abstract class MessageType<VariantType> where VariantType : MessageType<VariantType>
    {
        protected delegate string PrintMessage(VariantType obj, StringBuilder sb);
        protected delegate void RealizeMessage(VariantType obj, string[] msg);

        private static MessageTypeAttribute typeAttr;

        protected static PrintMessage DynamicToString = InitializeCustomToString();
        protected static RealizeMessage DynamicRealize = InitializeCustomRealize();


        /// <summary>
        /// The header of corresponding string message.
        /// </summary>
        public static string Header;

        /// <summary>
        /// Realize the value of object with a string.
        /// </summary>
        /// <param name="msg">Information of object in string.</param>
        public void Realize(string msg)
        {
            List<string> strList = new List<string>(5);
            string head = string.Empty;
            string tail = msg;
            char[] seperator1 = { '>' };
            string[] seperator2 = { "<@" };

            while (tail != string.Empty)
            {
                string[] splited = tail.Split(seperator1, 2);
                string value = splited[0].Split(seperator2, 2, StringSplitOptions.None).First();
                if (value != "") strList.Add(value);
                tail = splited[1];
            }

            DynamicRealize((VariantType)this, strList.ToArray());
        }

        /// <summary>
        /// Custom ToString() which serialize object to desire format.
        /// </summary>
        /// <returns>The result of serialization.</returns>
        public override string ToString()
        {
            return DynamicToString((VariantType)this, new StringBuilder(50));
        }

        /// <summary>
        /// Dynamically emit MSIL at runtime according to type's metadata.
        /// </summary>
        /// <returns>Delegate to generated code.</returns>
        private static PrintMessage InitializeCustomToString()
        {
            Type type = typeof(VariantType);
            FieldInfo[] fieldList = type.GetFields();

            typeAttr = type.GetCustomAttributes(typeof(MessageTypeAttribute), true).FirstOrDefault() as MessageTypeAttribute;
            if (typeAttr == null) throw new Exception(type.Name + " must be marked with MessageTypeAttribute.");
            Header = typeAttr.Header;

            DynamicMethod dm = new DynamicMethod("CustomPrintMessage", typeof(string), new Type[] { typeof(VariantType), typeof(StringBuilder) }, type);
            ILGenerator ig = dm.GetILGenerator(128);

            ig.Emit(OpCodes.Ldarg_1);
            ig.Emit(OpCodes.Ldstr, typeAttr.Header);
            ig.EmitCall(OpCodes.Call, StringBuilderType.GetAppendInfo(typeof(string)), null);

            foreach (var fi in fieldList)
            {
                MessageTagAttribute attr = fi.GetCustomAttributes(typeof(MessageTagAttribute), true).FirstOrDefault() as MessageTagAttribute;
                if (attr != null)
                {
                    if (fi.FieldType.IsPrimitive || fi.FieldType == typeof(string))
                    {
                        ig.Emit(OpCodes.Ldarg_0);
                        ig.Emit(OpCodes.Ldfld, fi);
                        ig.EmitCall(OpCodes.Call, StringBuilderType.GetAppendInfo(fi.FieldType), null);
                    }
                    else if (fi.FieldType == typeof(DateTime))
                    {
                        ig.Emit(OpCodes.Ldarg_0);
                        ig.Emit(OpCodes.Ldfld, fi);
                        ig.Emit(OpCodes.Box, typeof(DateTime));
                        ig.EmitCall(OpCodes.Call, StringBuilderType.GetAppendInfo(typeof(object)), null);
                    }
                    else if (fi.FieldType.IsArray)
                    {
                        ig.Emit(OpCodes.Ldarg_0);
                        ig.Emit(OpCodes.Ldfld, fi);
                        ig.EmitCall(OpCodes.Call, ConversionType.GetJoinArrayInfo(fi.FieldType), null);
                        ig.EmitCall(OpCodes.Call, StringBuilderType.GetAppendInfo(typeof(string)), null);
                    }
                    else
                    {
                        throw new NotImplementedException(fi.FieldType.Name + " is not supported by DynamicToString in MessageType");
                    }

                    ig.Emit(OpCodes.Ldstr, attr.Tag);
                    ig.EmitCall(OpCodes.Call, StringBuilderType.GetAppendInfo(typeof(string)), null);
                }
            }

            ig.Emit(OpCodes.Ldstr, typeAttr.EndMark);
            ig.EmitCall(OpCodes.Call, StringBuilderType.GetAppendInfo(typeof(string)), null);

            ig.EmitCall(OpCodes.Call, StringBuilderType.GetToStringInfo(), null);
            ig.Emit(OpCodes.Ret);

            return (PrintMessage)dm.CreateDelegate(typeof(PrintMessage));
        }

        /// <summary>
        /// Dynamically emit MSIL at runtime according to type's metadata.
        /// </summary>
        /// <returns>Delegate to generated code.</returns>
        private static RealizeMessage InitializeCustomRealize()
        {
            Type type = typeof(VariantType);
            FieldInfo[] fieldList = type.GetFields();
            int index = 0;

            DynamicMethod dm = new DynamicMethod("CustomRealizeMessage", typeof(void), new Type[] { typeof(VariantType), typeof(string[]) }, type);
            ILGenerator ig = dm.GetILGenerator(128);

            foreach (var fi in fieldList)
            {
                MessageTagAttribute attr = fi.GetCustomAttributes(typeof(MessageTagAttribute), true).FirstOrDefault() as MessageTagAttribute;
                if (attr != null)
                {
                    if (fi.FieldType.IsPrimitive || fi.FieldType == typeof(DateTime))
                    {
                        ig.Emit(OpCodes.Ldarg_0);
                        ig.Emit(OpCodes.Ldarg_1);
                        ig.Emit(OpCodes.Ldc_I4, index++);
                        ig.Emit(OpCodes.Ldelem, typeof(string));
                        ig.EmitCall(OpCodes.Call, ConversionType.GetParseInfo(fi.FieldType), null);
                        ig.Emit(OpCodes.Stfld, fi);
                    }
                    else if (fi.FieldType == typeof(string))
                    {
                        ig.Emit(OpCodes.Ldarg_0);
                        ig.Emit(OpCodes.Ldarg_1);
                        ig.Emit(OpCodes.Ldc_I4, index++);
                        ig.Emit(OpCodes.Ldelem, typeof(string));
                        ig.Emit(OpCodes.Stfld, fi);
                    }
                    else if (fi.FieldType.IsArray)
                    {
                        ig.Emit(OpCodes.Ldarg_0);
                        ig.Emit(OpCodes.Ldarg_1);
                        ig.Emit(OpCodes.Ldc_I4, index++);
                        ig.Emit(OpCodes.Ldelem, typeof(string));
                        ig.EmitCall(OpCodes.Call, ConversionType.GetArrayParseInfo(fi.FieldType), null);
                        ig.Emit(OpCodes.Stfld, fi);
                    }
                    else
                    {
                        throw new NotImplementedException(fi.FieldType.Name + " is not supported by DynamicRealize in MessageType");
                    }
                }
            }
            ig.Emit(OpCodes.Ret);
            return (RealizeMessage)dm.CreateDelegate(typeof(RealizeMessage));
        }
    }
}
