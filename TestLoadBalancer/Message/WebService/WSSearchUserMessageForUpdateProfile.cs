﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@WS_SEARCHUSERFORPROFILEUPDATE>", "<@EOF>")]
    class WSSearchUserMessageForUpdateProfile : MessageType<WSSearchUserMessageForUpdateProfile>
    {
        [MessageTag("<@FRIENDCHATUSERID>")]
        public string FriendChatUserID;

        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@PROFILENAME>")]
        public string ProfileName;

        [MessageTag("<@PROFILEIMAGEURL>")]
        public string ProfileImageURL;

        [MessageTag("<@STATUS>")]
        public string Status;

        [MessageTag("<@LASTUPDATEDPROFILEDATE>")]
        public string LastUpdatedProfileDate;
    }
}
