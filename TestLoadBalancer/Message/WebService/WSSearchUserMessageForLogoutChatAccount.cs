﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@WS_SEARCHUSERFORLOGOUTCHATACCOUNT>", "<@EOF>")]
    class WSSearchUserMessageForLogoutChatAccount : MessageType<WSSearchUserMessageForLogoutChatAccount>
    {
        [MessageTag("<@EXISTINGCHATUSERID>")]
        public string ExistingChatUserID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@OPERATINGTYPE>")]
        public int OperatingType;

        [MessageTag("<@DEVICETOKEN>")]
        public string DeviceToken;

    }
}
