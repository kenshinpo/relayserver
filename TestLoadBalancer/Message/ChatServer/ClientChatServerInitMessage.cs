﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CLIENT_CHATSERVER_INIT>", "<@EOF>")]
    class ClientChatServerInitMessage : MessageType<ClientChatServerInitMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;  
    }
}
