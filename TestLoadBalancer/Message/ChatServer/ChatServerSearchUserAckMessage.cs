﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERACK>", "<@EOF>")]
    class ChatServerSearchUserAckMessage : MessageType<ChatServerSearchUserAckMessage>
    {
        [MessageTag("<@STATUS>")]
        public string status = string.Empty;  
    }
}
