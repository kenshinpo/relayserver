﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CHATSERVER_INITACK>", "<@EOF>")]
    class ChatServerInitAckMessage : MessageType<ChatServerInitAckMessage>
    {
        [MessageTag("<@STATUS>")]
        public string status;
    }
}
