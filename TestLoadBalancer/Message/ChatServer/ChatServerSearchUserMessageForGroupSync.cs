﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERFORGROUPSYNC>", "<@EOF>")]
    class ChatServerSearchUserMessageForGroupSync : MessageType<ChatServerSearchUserMessageForGroupSync>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;

        [MessageTag("<@ADMINID>")]
        public string AdminID;

        [MessageTag("<@MEMBERSID>")]
        public string MembersID;

        [MessageTag("<@MEMBERSPROFILENAME>")]
        public string MembersProfileName;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
