﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERFORGROUPTITLEUPDATE>", "<@EOF>")]
    class ChatServerSearchUserMessageForGroupTitleUpdate : MessageType<ChatServerSearchUserMessageForGroupTitleUpdate>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
