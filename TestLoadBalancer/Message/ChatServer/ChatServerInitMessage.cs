﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CHATSERVER_INIT>", "<@EOF>")]
    class ChatServerInitMessage : MessageType<ChatServerInitMessage>
    {
        [MessageTag("<@IP>")]
        public string ipAddress = string.Empty;  
    }
}
