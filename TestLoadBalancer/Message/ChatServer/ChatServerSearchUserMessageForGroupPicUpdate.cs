﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERFORGROUPPICUPDATE>", "<@EOF>")]
    class ChatServerSearchUserMessageForGroupPicUpdate : MessageType<ChatServerSearchUserMessageForGroupPicUpdate>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
