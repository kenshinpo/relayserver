﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEND>", "<@EOF>")]
    class ChatServerSendMessage : MessageType<ChatServerSendMessage>
    {
        [MessageTag("<@message>")]
        public string message = string.Empty;
    }
}
