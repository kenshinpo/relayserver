﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@GAMESERVER_SEARCHUSERFORINVITESENT>", "<@EOF>")]
    class ChatServerSearchUserMessageForInviteSent : MessageType<ChatServerSearchUserMessageForInviteSent>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
