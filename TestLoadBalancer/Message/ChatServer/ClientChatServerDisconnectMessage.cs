﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadBalancer.Message.ChatServer
{
    [MessageType("<@CLIENT_CHATSERVER_DISCONNECT>", "<@EOF>")]
    class ClientChatServerDisconnectMessage : MessageType<ClientChatServerDisconnectMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;  
    }
}
