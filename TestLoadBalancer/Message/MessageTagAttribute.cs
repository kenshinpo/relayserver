﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadBalancer.Message
{
    /// <summary>
    /// Metadata type that provide information to serialize a field into string.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    class MessageTagAttribute : Attribute
    {
        /// <summary>
        /// The tag that append behind the field value.
        /// </summary>
        public string Tag { get; set; }

        public MessageTagAttribute(string tag)
            : base()
        {
            Tag = tag;
        }
    }
}
