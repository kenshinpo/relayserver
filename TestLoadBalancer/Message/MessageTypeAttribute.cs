﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadBalancer.Message
{
    /// <summary>
    /// Metadata type that provide header and end mark for serialization of an object into string.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    class MessageTypeAttribute : Attribute
    {
        public string Header { get; set; }
        public string EndMark { get; set; }

        public MessageTypeAttribute(string header, string endmark)
            : base()
        {
            Header = header;
            EndMark = endmark;
        }
    }
}
