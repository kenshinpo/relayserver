﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using LoadBalancer.Message.ChatServer;
using LoadBalancer.Service;
using LoadBalancer.Utilities;
using LoadBalancer;
using TestLoadBalancer;


namespace LoadBalancer.Connectivity
{
    /// <summary>
    /// Host application service with TCP protocol, listen and accept new connection from clients.
    /// </summary>
    public class ServiceHost
    {
        private IProtocolFactory serviceFactory;
        private int port;
        private Socket listener;
        private Thread listenThread;
        private bool isStartSocket = false;

        /// <summary>
        /// Current IP address and port. Available only when the instance is running.
        /// </summary>
        public IPEndPoint ListeningEndPoint { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="port">The port that this instance listen on.</param>
        /// <param name="serviceFactory">A factory that spawn service object when new connection is accepted.</param>
        public ServiceHost(int port, IProtocolFactory serviceFactory)
        {
            this.port = port;
            this.serviceFactory = serviceFactory;
        }

        /// <summary>
        /// Start to accept new connection.
        /// </summary>
        public void Run()
        {
            if (listener == null)
            {
                isStartSocket = true;
                ListeningEndPoint = ResolveEndPoint();
                listener = new Socket(ListeningEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(ListeningEndPoint);
                listener.Listen(1024);
                serviceFactory.Start();
                listenThread = new Thread(ListenRoutine);
                listenThread.Start();
                return;
            }
            throw new InvalidOperationException("ServiceHost.Start occurs error: ServiceHost is already running.");
        }

        /// <summary>
        /// Stop running.
        /// </summary>
        public void Stop()
        {
            if (listener != null)
            {
                isStartSocket = false;
                listener.Close();
                serviceFactory.Stop();
                return;
            }
            throw new InvalidOperationException("ServiceHost.Stop occurs error: ServiceHost is not started.");
        }

        private void ListenRoutine()
        {
            try
            {
                while (isStartSocket)
                {
                    var asyncResult = listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                    asyncResult.AsyncWaitHandle.WaitOne();
                }

            }
            catch (Exception ex)
            {
                Form1.ShowText(String.Format("ServiceHost.ListenRoutine occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                if (listener == null || isStartSocket == false)
                    return;

                Socket pSocket = listener.EndAccept(ar);
                IProtocol service = serviceFactory.CreateService();
                TcpTransport transport = new TcpTransport(pSocket, service);
                Form1.ShowText("ServiceHost || AcceptCallback");
                service.ConnectionMade(transport);

            }
            catch (Exception ex)
            {
                Form1.ShowText(String.Format("ServiceHost.AcceptCallback occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        public void sendMessageToIntendedChatServer(string ipAddress, string message)
        {
            ServerSessionCollection onlineChatServers = serviceFactory.GetOnlineChatServers();

            Form1.ShowText("Number of chat servers: " + onlineChatServers.Count());

            ServerSession chatServerToBeDispatched = onlineChatServers[ipAddress];

            if (chatServerToBeDispatched != null)
            {
                Form1.ShowText("Message to be sent to ChatServer: " + chatServerToBeDispatched.ipAddress);
                try
                {
                    var str = message.ToString();
                    chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(message.ToString()));
                }
                catch (Exception ex)
                {
                    Form1.ShowText(String.Format("Server: {0} session occurs error: {1} \nCall Stack:\n{2}", chatServerToBeDispatched.ipAddress, ex.Message, ex.StackTrace));
                    //chatServerToBeDispatched.Transport.Close();
                }
            }
        }

        private IPEndPoint ResolveEndPoint()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            int ipv4Index = 0;

            for (int index = 0; index < ipHostInfo.AddressList.Length; index++)
            {
                IPAddress addressFromLocal = ipHostInfo.AddressList[index];

                if (addressFromLocal.AddressFamily == AddressFamily.InterNetwork && addressFromLocal.ToString().StartsWith("192"))
                {
                    ipv4Index = index;
                    break;
                }
            }

            IPAddress ipAddress2 = ipHostInfo.AddressList[ipv4Index];
            Form1.ShowText("LB current address: " + ipAddress2.ToString());
            return new IPEndPoint(ipAddress2, port);
        }
    }
}
