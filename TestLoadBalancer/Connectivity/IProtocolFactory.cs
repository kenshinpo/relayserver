﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoadBalancer.Service;

namespace LoadBalancer.Connectivity
{
    /// <summary>
    /// The type which implemented this interface can spawn a object to cater client connection.
    /// </summary>
    public interface IProtocolFactory
    {
        /// <summary>
        /// Startup the factory, allows the factory to initialize itself.
        /// </summary>
        void Start();

        /// <summary>
        /// Spawn a new service object.
        /// </summary>
        /// <returns></returns>
        IProtocol CreateService();

        /// <summary>
        /// Tear down the factory.
        /// </summary>
        void Stop();

        /// <summary>
        /// Returns onlineChatServers
        /// </summary>
        ServerSessionCollection GetOnlineChatServers();
    }
}
