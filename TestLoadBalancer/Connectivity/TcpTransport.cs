﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LoadBalancer.Utilities;
using System.IO;

using System.Net;
using LoadBalancer;
using TestLoadBalancer;

namespace LoadBalancer.Connectivity
{
    /// <summary>
    /// Tcp protocol implementation of ITransport. Provide read and write operation to TCP socket.
    /// </summary>
    class TcpTransport : ITransport
    {
        private class FileTransferState
        {
            public string filePath;
            public byte[] buffer;
            public int processedCount;
            public object state;
            public Action<object> callback;
        }

        private static readonly TimeSpan TIME_OUT = TimeSpan.FromMinutes(0.5f);
        private static readonly TimeSpan FILE_TIME_OUT = TimeSpan.FromMinutes(20.0f);
        private static readonly MemoryPool<byte> pool = new MemoryPool<byte>(512);
        internal Socket socket;
        internal IProtocol service;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="socket">The socket which the object wrap around.</param>
        /// <param name="service">The service object which the object is bound.</param>
        public TcpTransport(Socket socket, IProtocol service)
        {
            this.socket = socket;
            this.service = service;
        }

        /// <summary>
        /// Write data to TCP connection.
        /// </summary>
        /// <param name="data">Array of bytes to be written.</param>
        /// <param name="offset">Offset of data to be written with respect to the first byte in the data array.</param>
        /// <param name="length">Length of data to be written</param>
        public void Write(byte[] data, int offset = 0, int length = 0)
        {
            if (socket != null && socket.Connected)
            {
                var asyncResult = socket.BeginSend(data, offset, length, SocketFlags.None, WriteCallback, data);
                ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, TimeoutCallback, null, -1, true);
            }
        }

        /// <summary>
        /// Write data to TCP connection.
        /// </summary>
        /// <param name="data">Array of bytes to be written.</param>
        public void Write(byte[] data)
        {
            if (data != null)
            {
                IPEndPoint remoteIpEndPoint = socket.RemoteEndPoint as IPEndPoint;
                if (socket != null && socket.Connected)
                {
                    Form1.ShowText("Data written: " + data.ToString());

                    var asyncResult = socket.BeginSend(data, 0, data.Length, SocketFlags.None, WriteCallback, data);
                    ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, TimeoutCallback, null, -1, true);
                }
            }

        }

        /// <summary>
        /// Write a file to TCP connection.
        /// </summary>
        /// <param name="filePath">The file location at local machine.</param>
        /// <param name="state">User define state object which to be returned by callback method later.</param>
        /// <param name="callback">The method to be called when the transfer is complete.</param>
        public void WriteFile(string filePath, object state, Action<object> callback)
        {
            if (socket != null && socket.Connected)
            {
                var asyncState = new FileTransferState() { filePath = filePath, state = state, callback = callback };
                var asyncResult = socket.BeginSendFile(filePath, WriteFileCallback, asyncState);
                ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, TimeoutCallback, null, -1, true);
            }
        }

        /// <summary>
        /// Read data from TCP connection.
        /// </summary>
        /// <returns>A byte array of data.</returns>
        public byte[] Read()
        {
            if (socket != null || socket.Connected)
            {
                byte[] buffer = pool.Allocate();
                var asyncResult = socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadCallback, buffer);
                ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, TimeoutCallback, buffer, -1, true);
                return buffer;
            }
            return null;
        }

        /// <summary>
        /// Read a file from TCP connection.
        /// </summary>
        /// <param name="filePath">Path to save the file.</param>
        /// <param name="length">Expected length of the file.</param>
        /// <param name="state">User defined state which is return by callback later.</param>
        /// <param name="callback">The callback method to be called when the transfer is done.</param>
        public void ReadFile(string filePath, int length, object state, Action<object> callback)
        {
            if (socket != null || socket.Connected)
            {
                byte[] buffer = new byte[length];
                var asyncState = new FileTransferState()
                {
                    state = state,
                    filePath = filePath,
                    buffer = buffer,
                    callback = callback,
                    processedCount = 0
                };
                var asyncResult = socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadFileCallback, asyncState);
                ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, TimeoutCallback, buffer, -1, true);
            }
        }

        /// <summary>
        /// Shutdown and close the TCP connection.
        /// </summary>
        public void Close()
        {
            if (socket != null)
            {
                service.ConnectionLost(this);
                if (socket.Connected) socket.Shutdown(SocketShutdown.Both);
                socket.Close();
                socket = null;
            }
        }

        /// <summary>
        /// Check if is still connected. (unreliable)
        /// </summary>
        public bool IsValid()
        {
            return socket.Connected;
        }

        /// <summary>
        /// Return the underlying socket.
        /// </summary>
        /// <returns>An object that can be cast to .NET Socket type.</returns>
        public object GetHandle()
        {
            return socket;
        }

        private void WriteCallback(IAsyncResult ar)
        {
            try
            {
                if (socket == null) return;
                byte[] data = ar.AsyncState as byte[];
                int byteSent = socket.EndSend(ar);
                service.DataSent(data, byteSent);
            }
            catch (Exception ex)
            {
                Form1.ShowText(String.Format("TcpTransport.WriteCallback occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
                Close();
            }
        }

        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                if (socket == null) return;
                byte[] buffer = (byte[])ar.AsyncState;
                int byteReceived = socket.EndReceive(ar);
                if (byteReceived != 0)
                {
                    service.DataReceived(buffer, byteReceived);
                    pool.Recycle(buffer, byteReceived);
                }
                else
                {
                    Form1.ShowText("TcpTransport.ReadCallback: Remote user closed the socket.");
                    Close();
                }

            }
            catch (Exception ex)
            {
                Form1.ShowText(String.Format("TcpTransport.ReadCallback occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
                Close();
            }
        }

        private void WriteFileCallback(IAsyncResult ar)
        {
            try
            {
                if (socket == null) return;
                socket.EndSendFile(ar);
                FileTransferState state = (FileTransferState)ar.AsyncState;
                state.callback(state.state);
            }
            catch (Exception ex)
            {
                Form1.ShowText(String.Format("TcpTransport.WriteFileCallback occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
                Close();
            }
        }

        private void ReadFileCallback(IAsyncResult ar)
        {
            try
            {
                if (socket == null) return;
                FileTransferState state = (FileTransferState)ar.AsyncState;
                int byteReceived = socket.EndSend(ar);
                if (byteReceived != 0)
                {
                    state.processedCount += byteReceived;

                    if (state.processedCount == state.buffer.Length)
                    {
                        using (FileStream fs = new FileStream(state.filePath, FileMode.CreateNew))
                        {
                            fs.Write(state.buffer, 0, state.buffer.Length);
                            fs.Flush();
                            fs.Close();
                        }

                        state.callback(state.state);
                    }
                    else
                    {
                        int remaining = state.buffer.Length - state.processedCount;
                        var asyncResult = socket.BeginReceive(state.buffer, state.processedCount,
                            remaining, SocketFlags.None, ReadFileCallback, state);
                        //ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, TimeoutCallback, state.buffer, TIME_OUT, true);
                        if (!asyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromMinutes(3.0d))) TimeoutCallback(state, true);
                    }
                }
                else
                {
                    Form1.ShowText("TcpTransport.ReadFileCallback: Remote user closed the socket.");
                    Close();
                }

            }
            catch (Exception ex)
            {
                Form1.ShowText(String.Format("TcpTransport.ReadFileCallback occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
                Close();
            }
        }

        private void TimeoutCallback(object state, bool timedOut)
        {
            if (timedOut)
            {
                Form1.ShowText("TcpTransport.TimeoutCallback: Connection timeout.");
                service.ConnectionTimeOut(this);
            }
        }
    }
}
