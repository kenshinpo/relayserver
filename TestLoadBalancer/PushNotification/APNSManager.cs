﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Android;
using System.Diagnostics;
using TestLoadBalancer;

namespace CherryChat.Connectivity.PushNotification
{
    class APNSManager
    {
        //Singleton implementation
        protected static APNSManager m_pSingleton = null;

        //Apple Notification Broker
        protected PushBroker m_pBroker_Apple;

        //Google Notification Broker
        protected PushBroker m_pBroker_Google;

        //Operation Mode to load the right certificate.
        public enum OPERATION_MODE{
            OPS_DEV_RHS = 0,        //Development Mode in RHS
            OPS_PRO_RHS = 1,        //Production Mode in RHS
            OPS_DEV_CC = 2,         //Development Mode in Cherry Credit
            OPS_PRO_CC = 3,         //Production Mode in Cherry Credit
        };

        /// <summary>
        /// Start the APNS "Manager
        /// </summary>
        /// <returns></returns>
        public static APNSManager startManager(OPERATION_MODE mode)
        {
            if (m_pSingleton == null) 
                m_pSingleton = new APNSManager(mode);

            return m_pSingleton;
        }

        /// <summary>
        /// Get an instance of the APNS Manager
        /// </summary>
        /// <returns></returns>
        public static APNSManager getInstance()
        {
            return m_pSingleton;
        }

        protected APNSManager(OPERATION_MODE mode)
        {
            //Create our push services broker
            m_pBroker_Apple = new PushBroker();
            
            //Wire up the events for all the services that the broker registers
            m_pBroker_Apple.OnNotificationSent += NotificationSent;
            m_pBroker_Apple.OnChannelException += ChannelException;
            m_pBroker_Apple.OnServiceException += ServiceException;
            m_pBroker_Apple.OnNotificationFailed += NotificationFailed;
            m_pBroker_Apple.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            m_pBroker_Apple.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            //m_pBroker_Apple.OnChannelCreated += ChannelCreated;
            //m_pBroker_Apple.OnChannelDestroyed += ChannelDestroyed;

            //-------------------------
            // APPLE NOTIFICATIONS
            //-------------------------
            //Configure and start Apple APNS
            // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
            //   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
            //   app with!
        
            //IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
            //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
            //  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
            //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')

            
            if(mode == OPERATION_MODE.OPS_DEV_RHS){
                var appleCert = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "apns/CC.APNS.DEV.p12"));
                m_pBroker_Apple.RegisterAppleService(new ApplePushChannelSettings(appleCert, "cctest8888")); //Extension method
            }
            else if(mode == OPERATION_MODE.OPS_PRO_RHS){
                var appleCert = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "apns/CC.APNS.PRO.p12"));
                m_pBroker_Apple.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "cctest8888")); //Extension method
            }
            else if(mode == OPERATION_MODE.OPS_DEV_CC){
                var appleCert = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "apns/cherrychat.development.p12"));
                m_pBroker_Apple.RegisterAppleService(new ApplePushChannelSettings(appleCert, "cherry@chat")); //Extension method
            }
            else if(mode == OPERATION_MODE.OPS_PRO_CC){
                var appleCert = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "apns/cherrychat.production.p12"));
                m_pBroker_Apple.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "cherry@chat")); //Extension method
            }

            //-------------------------
            // GOOGLE NOTIFICATIONS
            //-------------------------
            m_pBroker_Google = new PushBroker();
            //Wire up the events for all the services that the broker registers
            m_pBroker_Google.OnNotificationSent += NotificationSent;
            m_pBroker_Google.OnChannelException += ChannelException;
            m_pBroker_Google.OnServiceException += ServiceException;
            m_pBroker_Google.OnNotificationFailed += NotificationFailed;
            m_pBroker_Google.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            m_pBroker_Google.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            //m_pBroker_Google.OnChannelCreated += ChannelCreated;
            //m_pBroker_Google.OnChannelDestroyed += ChannelDestroyed;


            m_pBroker_Google.RegisterGcmService(new GcmPushChannelSettings("991390146416", "AIzaSyCbX4Rs6MbI-NSAqAwlDaTLQX1VKaXN2Ak", "com.cherrycredits.cherrychat"));
            //m_pBroker_Google.RegisterGcmService(new GcmPushChannelSettings("991390146416", "AIzaSyBn8B9UMkE40418VJK8dhNEnVm9gkfmVLs", "com.cherrycredits.cherrychat"));

            

        }

        /**
         *  Sends an APNS notification when the user's phone is offline.
         */ 
        public void SendNotification(string deviceToken, string msg, string chatId, int badge, int operatingType){
            //Fluent construction of an iOS notification
            //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
            //  for registered for remote notifications is called, and the device token is passed back to you
            //m_pBroker.QueueNotification(new AppleNotification()
            //                           .ForDeviceToken("5af21046a38b01dc4dd42d75311ed967d6fc8c84cf2b39862a9f1dc3d641067a")
            //                           .WithAlert("Hello World!")
            //                           .WithBadge(1)
            //                           .WithSound("sound.caf"));

            if (operatingType == 1)//iOS
            {
                Form1.ShowText("Push APPLE notification"); 

                m_pBroker_Apple.QueueNotification(new AppleNotification()
                                       .ForDeviceToken(deviceToken)
                                       .WithAlert(msg)
                                       .WithBadge(badge)
                                       .WithSound("default")
                                       .WithCustomItem("chatID", chatId));

            }
            else if (operatingType == 2) //android
            {
                Form1.ShowText("Token: " + deviceToken);
                Form1.ShowText("operatingType: android -> " + @"{""alert"": """ + msg + @""",""chatId"":""" + chatId + @""",""badge"":""" + badge + @""",""sound"":""default""}");

                //Logger.Debug("operatingType: android -> " + @"{""alert"": """ + msg + @""",""chatId"":""" + chatId + @""",""badge"":""" + badge + @""",""sound"":""default""}");
                //Logger.Debug("Device Token: " + deviceToken);
                m_pBroker_Google.QueueNotification(new GcmNotification().ForDeviceRegistrationId(deviceToken).WithJson(@"{""alert"": """ + msg + @""",""chatId"":""" + chatId + @""",""badge"":""" + badge + @""",""sound"":""default""}"));
                //Logger.Debug("GCM Done");
            }
            
        }
        
        static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Currently this event will only ever happen for Android GCM
            //Debug.WriteLine("Device Registration Changed:  Old-> " + oldSubscriptionId + "  New-> " + newSubscriptionId + " -> " + notification);
        }

        static void NotificationSent(object sender, INotification notification)
        {
            //Debug.WriteLine("Sent: " + sender + " -> " + notification);
            Form1.ShowText("Sent: " + sender + " -> " + notification);
        }

        static void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            //Debug.WriteLine("Failure: " + sender + " -> " + notificationFailureException.Message + " -> " + notification);
            Form1.ShowText("Failure: " + sender + " -> " + notificationFailureException.Message + " -> " + notification);
        }

        static void ChannelException(object sender, IPushChannel channel, Exception exception)
        {
            //Debug.WriteLine("Channel Exception: " + sender + " -> " + exception);
            Form1.ShowText("Channel Exception: " + sender + " -> " + exception);
        }

        static void ServiceException(object sender, Exception exception)
        {
            //Debug.WriteLine("Channel Exception: " + sender + " -> " + exception);
            Form1.ShowText("Channel Exception: " + sender + " -> " + exception);
        }

        static void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            //Debug.WriteLine("Device Subscription Expired: " + sender + " -> " + expiredDeviceSubscriptionId);
            Form1.ShowText("Device Subscription Expired: " + sender + " -> " + expiredDeviceSubscriptionId);
        }

        static void ChannelDestroyed(object sender)
        {
            //Debug.WriteLine("Channel Destroyed for: " + sender);
            Form1.ShowText("Channel Destroyed for: " + sender);
        }

        static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //Debug.WriteLine("Channel Created for: " + sender);
            Form1.ShowText("Channel Created for: " + sender);
        }
	}
}
