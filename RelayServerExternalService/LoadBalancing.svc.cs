﻿using System.ServiceModel.Activation;
using RelayServerCore.LoadBalancing;

namespace RelayServerExternalService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LoadBalancing : ILoadBalancing
    {
        public void DoWork()
        {
        }

        public string GetLowestLoadServerIP()
        {
            return ServerManager.GetLowestLoadServerHostname();
        }
    }
}