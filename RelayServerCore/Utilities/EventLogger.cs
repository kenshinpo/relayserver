﻿using System.Diagnostics;

namespace RelayServerCore.Utilities
{
    static class EventLogger
    {
        public static string EVENTLOGGER_SOURCE = "IP Tracker Server Source";
        public static string EVENTLOGGER_LOG_NAME = "IP Tracker Service Log";

        private static EventLog eventLog;

        static EventLogger()
        {
            if (!EventLog.SourceExists(EVENTLOGGER_SOURCE))
                EventLog.CreateEventSource(EVENTLOGGER_SOURCE, EVENTLOGGER_LOG_NAME);
            eventLog = new EventLog(EventLogger.EVENTLOGGER_LOG_NAME);
            eventLog.Source = EVENTLOGGER_SOURCE;
        }

        public static void Info(string message)
        {
            eventLog.WriteEntry(message, EventLogEntryType.Information);
        }

        public static void Error(string message)
        {
            eventLog.WriteEntry(message, EventLogEntryType.Error);
        }

        public static void Warning(string message)
        {
            eventLog.WriteEntry(message, EventLogEntryType.Warning);
        }
    }
}
