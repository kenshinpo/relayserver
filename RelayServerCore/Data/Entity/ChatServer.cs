﻿using System;
using System.Collections.Generic;

namespace RelayServerCore.Data.Entity
{
    [Serializable]
    public class ChatServer
    {
        public string hostname { get; set; }
        public List<string> onlineUsers { get; set; }
    }
}