﻿using System;
using System.Collections.Generic;

namespace RelayServerCore.Data.Entity
{
    [Serializable]
    public class OnlineCounter
    {
        public Dictionary<string, int> recorder { get; set; }
    }
}