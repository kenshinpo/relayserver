﻿using System;

namespace RelayServerCore.Data.Entity
{
    [Serializable]
    public class CherryChatUser
    {
        public string cuid { get; set; }
        public string chatServerHostname { get; set; }
    }
}