﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@WS_SEARCHUSERFORADDFRIEND>", "<@EOF>")]
    class WSSearchUserMessageForAddFriend : MessageType<WSSearchUserMessageForAddFriend>
    {
        [MessageTag("<@FRIENDCHATUSERID>")]
        public string FriendChatUserID;

        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@PROFILENAME>")]
        public string ProfileName;

        [MessageTag("<@PROFILEIMAGEURL>")]
        public string ProfileImageURL;

        [MessageTag("<@STATUS>")]
        public string Status;

        [MessageTag("<@LASTUPDATEDPROFILEDATE>")]
        public string LastUpdatedProfileDate;

        [MessageTag("<@RELATIONSHIPTYPE>")]
        public int RelationshipType;
    }
}
