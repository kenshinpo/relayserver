﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@WS_SEARCHUSERFORINVALIDUPDATE>", "<@EOF>")]
    class WSSearchUserMessageForUpdateInvalid : MessageType<WSSearchUserMessageForUpdateInvalid>
    {
        [MessageTag("<@FRIENDCHATUSERID>")]
        public string FriendChatUserID;

        [MessageTag("<@INVALIDCHATUSERID>")]
        public string InvalidChatUserID;

        [MessageTag("<@NEWCHATUSERID>")]
        public string NewChatUserID;

        [MessageTag("<@COUNTRYCODE>")]
        public string CountryCode;

        [MessageTag("<@PHONENUMBER>")]
        public string PhoneNumber;

        [MessageTag("<@PROFILENAME>")]
        public string ProfileName;

        [MessageTag("<@PROFILEIMAGEURL>")]
        public string ProfileImageURL;

        [MessageTag("<@STATUS>")]
        public string Status;

        [MessageTag("<@LASTUPDATEDPROFILEDATE>")]
        public string LastUpdatedProfileDate;
    }
}
