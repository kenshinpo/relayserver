﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@WS_SEARCHUSERFORLOGOUTCHERRYACCOUNT>", "<@EOF>")]
    class WSSearchUserMessageForLogoutCherryAccount : MessageType<WSSearchUserMessageForLogoutCherryAccount>
    {
        [MessageTag("<@EXISTINGCHATUSERID>")]
        public string ExistingChatUserID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@OPERATINGTYPE>")]
        public int OperatingType;

        [MessageTag("<@DEVICETOKEN>")]
        public string DeviceToken;
    }
}
