﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CherryChat.Message
{
    class MessageConverter
    {

        public static string HtmlEncode(string plainText)
        {
            string HTMLEncodedText = HttpUtility.HtmlEncode(plainText);
            return HtmlCustomEncode(HTMLEncodedText);
        }

        public static string HtmlCustomEncode(string plainText)
        {
            plainText = plainText.Replace(",", "&#44;");
            return plainText;
        }

        public static string HtmlDecode(string plainText)
        {
            string HTMLEncodedText = HttpUtility.HtmlDecode(plainText);
            return HtmlCustomDecode(HTMLEncodedText);
        }

        public static string HtmlCustomDecode(string plainText)
        {
            plainText = plainText.Replace("&#44;", ",");
            return plainText;
        }
    }
}
