﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERFORRECV>", "<@EOF>")]
    class ChatServerSearchUserMessageForRecv : MessageType<ChatServerSearchUserMessageForRecv>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@BADGE>")]
        public int Badge;

        [MessageTag("<@OPERATINGTYPE>")]
        public int OperatingType;

        [MessageTag("<@DEVICETOKEN>")]
        public string DeviceToken;
    }
}
