﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@GAMESERVER_SEARCHUSERFORGIFTSENT>", "<@EOF>")]
    class ChatServerSearchUserMessageForGiftSent : MessageType<ChatServerSearchUserMessageForGiftSent>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
