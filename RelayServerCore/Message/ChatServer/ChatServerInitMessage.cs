﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CHATSERVER_INIT>", "<@EOF>")]
    class ChatServerInitMessage : MessageType<ChatServerInitMessage>
    {
        [MessageTag("<@IP>")]
        public string ipAddress = string.Empty;  
    }
}
