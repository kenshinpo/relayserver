﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CHATSERVER_INITACK>", "<@EOF>")]
    class ChatServerInitAckMessage : MessageType<ChatServerInitAckMessage>
    {
        [MessageTag("<@STATUS>")]
        public string status;
    }
}
