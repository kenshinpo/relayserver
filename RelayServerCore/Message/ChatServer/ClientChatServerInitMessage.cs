﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CLIENT_CHATSERVER_INIT>", "<@EOF>")]
    class ClientChatServerInitMessage : MessageType<ClientChatServerInitMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;  
    }
}
