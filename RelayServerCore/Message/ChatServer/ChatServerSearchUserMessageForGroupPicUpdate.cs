﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERFORGROUPPICUPDATE>", "<@EOF>")]
    class ChatServerSearchUserMessageForGroupPicUpdate : MessageType<ChatServerSearchUserMessageForGroupPicUpdate>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;

        [MessageTag("<@NOTIFICATIONID>")]
        public string NotificationID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
