﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERACK>", "<@EOF>")]
    class ChatServerSearchUserAckMessage : MessageType<ChatServerSearchUserAckMessage>
    {
        [MessageTag("<@STATUS>")]
        public string status = string.Empty;  
    }
}
