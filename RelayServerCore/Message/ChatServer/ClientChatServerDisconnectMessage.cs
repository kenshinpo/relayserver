﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CLIENT_CHATSERVER_DISCONNECT>", "<@EOF>")]
    class ClientChatServerDisconnectMessage : MessageType<ClientChatServerDisconnectMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;  
    }
}
