﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEARCHUSERFORGROUPTITLEUPDATE>", "<@EOF>")]
    class ChatServerSearchUserMessageForGroupTitleUpdate : MessageType<ChatServerSearchUserMessageForGroupTitleUpdate>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@NOTIFICATIONID>")]
        public string NotificationID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
