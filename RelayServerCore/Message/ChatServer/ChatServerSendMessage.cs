﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@CHATSERVER_SEND>", "<@EOF>")]
    class ChatServerSendMessage : MessageType<ChatServerSendMessage>
    {
        [MessageTag("<@message>")]
        public string message = string.Empty;
    }
}
