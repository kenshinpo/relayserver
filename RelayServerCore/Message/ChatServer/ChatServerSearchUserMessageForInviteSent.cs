﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@GAMESERVER_SEARCHUSERFORINVITESENT>", "<@EOF>")]
    class ChatServerSearchUserMessageForInviteSent : MessageType<ChatServerSearchUserMessageForInviteSent>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
