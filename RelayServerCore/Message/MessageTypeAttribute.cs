﻿using System;

namespace RelayServerCore.Message
{
    /// <summary>
    /// Metadata type that provide header and end mark for serialization of an object into string.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    class MessageTypeAttribute : Attribute
    {
        public string Header { get; set; }
        public string EndMark { get; set; }

        public MessageTypeAttribute(string header, string endmark)
            : base()
        {
            Header = header;
            EndMark = endmark;
        }
    }
}
