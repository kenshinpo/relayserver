﻿namespace RelayServerCore.Message.ChatServer
{
    [MessageType("<@GAMESERVER_SEARCHUSERFORINVITESEND>", "<@EOF>")]
    class GameServerSearchUserMessageForInviteSend : MessageType<GameServerSearchUserMessageForInviteSend>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
