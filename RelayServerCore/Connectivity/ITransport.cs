﻿namespace RelayServerCore.Connectivity
{
    /// <summary>
    /// The type which implemented this interface provides basic communication functionality.
    /// </summary>
    public interface ITransport
    {
        /// <summary>
        /// Write data to transportation medium.
        /// </summary>
        /// <param name="data">Array of bytes to be written.</param>
        /// <param name="offset">Offset of data to be written with respect to the first byte in the data array.</param>
        /// <param name="length">Length of data to be written</param>
        void Write(byte[] data, int offset = 0, int length = 0);

        /// <summary>
        /// Write data to transportation medium.
        /// </summary>
        /// <param name="data">Array of bytes to be written.</param>
        void Write(byte[] data);

        /// <summary>
        /// Read data from transportation medium.
        /// </summary>
        /// <returns>A byte array of data.</returns>
        byte[] Read();

        /// <summary>
        /// Close the transportation medium.
        /// </summary>
        void Close();

        /// <summary>
        /// Check if the transportation medium is still valid for read and write.
        /// </summary>
        /// <returns>True if the transportation is valid, false otherwise.</returns>
        bool IsValid();

        /// <summary>
        /// Get the underlying object that this interface wrapped around.
        /// </summary>
        /// <returns>The underlying object. Can be casted to appropiate type.</returns>
        object GetHandle();
    }
}
