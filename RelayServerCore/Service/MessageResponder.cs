﻿using System;
using System.Text;
using RelayServerCore.Message;

namespace RelayServerCore.Service
{
    /// <summary>
    /// Reponse type to particular user request.
    /// </summary>
    public abstract class MessageResponder
    {
        /// <summary>
        /// The header of message that this object is interested.
        /// </summary>
        public string MessageOfInterested { get; protected set; }

        /// <summary>
        /// Handle and respond to chat server request.
        /// </summary>
        /// <param name="currentServer">The sender of request.</param>
        /// <param name="message">Current request.</param>
        /// <returns>Indicate the server should continue reading.</returns>
        public abstract void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message);

        protected T RealizeMessage<T>(string textMsg) where T : MessageType<T>, new()
        {
                T msg = new T();
                msg.Realize(textMsg);
                return msg;
        }

        protected void ReplyMessage<T>(ServerSession server, MessageType<T> msg) where T : MessageType<T>, new()
        {
            try
            {
                var str = msg.ToString();
                server.Transport.Write(Encoding.UTF8.GetBytes(msg.ToString()));
            }
            catch (Exception ex)
            {
                 UB.LogFile.Append(@"D:\Logs\",String.Format("Server: {0} session occurs error: {1} \nCall Stack:\n{2}", server.ipAddress, ex.Message, ex.StackTrace));
                server.Transport.Close();
            }
        }
    }
}
