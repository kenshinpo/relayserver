﻿using System;
using System.Linq;
using System.Text;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace RelayServerCore.Service.ForChatServerResponder
{
    public class GameServerSearchUserResponderForGiftSend : MessageResponder
    {
        public GameServerSearchUserResponderForGiftSend()
            : base()
        {
            MessageOfInterested = MessageFormat.GAMESERVER_SEARCHUSERFORGIFTSEND.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                 // UB.LogFile.Append(@"D:\Logs\","Message for gift send: " + message);

                GameServerSearchUserMessageForGiftSend msg = RealizeMessage<GameServerSearchUserMessageForGiftSend>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(msg.ChatUserID);
                if (ccuser != null)
                {
                     // UB.LogFile.Append(@"D:\Logs\",msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@SENDGIFT>" + msg.ChatID + "<@CHATID>" + msg.MsgID + "<@MSGID>" + msg.RecvChatUserID + "<@RECVCHATUSERID>" + msg.Message + "<@MESSAGE>" + msg.Type + "<@TYPE>" + msg.Timestamp + "<@TIMESTAMP>" + "<@EOF>";
                         // UB.LogFile.Append(@"D:\Logs\","Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
               
            }
            catch (Exception ex)
            {
                 UB.LogFile.Append(@"D:\Logs\",ex.ToString());
            }
        }
    }
}
