﻿using System;
using System.Linq;
using System.Text;
using CherryChat.Connectivity.PushNotification;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;
using CherryChat.Message;

namespace RelayServerCore.Service.ForChatServerResponder
{
    public class GameServerSearchUserResponderForGiftRecv : MessageResponder
    {
        public GameServerSearchUserResponderForGiftRecv()
            : base()
        {
            MessageOfInterested = MessageFormat.GAMESERVER_SEARCHUSERFORGIFTRECV.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                 // UB.LogFile.Append(@"D:\Logs\","Message for gift recv: " + message);

                GameServerSearchUserMessageForGiftRecv msg = RealizeMessage<GameServerSearchUserMessageForGiftRecv>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(msg.ChatUserID);
                if (ccuser != null)
                {
                     // UB.LogFile.Append(@"D:\Logs\",msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@RECVGIFT>" + msg.ChatID + "<@CHATID>" + msg.MsgID + "<@MSGID>" + msg.SenderChatUserID + "<@SENDERCHATUSERID>" + msg.Message + "<@MESSAGE>" + msg.Type + "<@TYPE>" + msg.Timestamp + "<@TIMESTAMP>" + "<@EOF>";
                         // UB.LogFile.Append(@"D:\Logs\","Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
                else
                {
                     // UB.LogFile.Append(@"D:\Logs\",msg.ChatUserID + " is not at Memcache server. (OFFLINE)");

                    // Send notification 
                     // UB.LogFile.Append(@"D:\Logs\","Push notification");

                    if (!string.IsNullOrWhiteSpace(msg.NotificationMessage) && !string.IsNullOrWhiteSpace(msg.DeviceToken))
                    {
                        if (msg.NotificationMessage.Length > 100) msg.NotificationMessage = msg.NotificationMessage.Substring(0, 97) + "...";
                        APNSManager.getInstance().SendNotification(msg.DeviceToken, MessageConverter.HtmlDecode(msg.NotificationMessage), msg.ChatID, msg.Badge, msg.OperatingType);
                    }
                }
            }
            catch (Exception ex)
            {
                 UB.LogFile.Append(@"D:\Logs\",ex.ToString());
            }
        }
    }
}
