﻿using System;
using System.Linq;
using System.Text;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace RelayServerCore.Service.ForChatServerResponder
{
    public class ChatServerSearchUserResponderForGroupSync : MessageResponder
    {
        public ChatServerSearchUserResponderForGroupSync()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_SEARCHUSERFORGROUPSYNC.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                UB.LogFile.Append(@"D:\Logs\", "Message for group sync: " + message);

                ChatServerSearchUserMessageForGroupSync msg = RealizeMessage<ChatServerSearchUserMessageForGroupSync>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                    // UB.LogFile.Append(@"D:\Logs\",msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    // Ask intended chat server to send message
                    ServerSession chatServerToBeDispatched = onlineChatServers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@GROUPSYNC>" + msg.ChatID + "<@CHATID>" + msg.Title + "<@TITLE>" + msg.GroupPicURL + "<@GROUPPICURL>" + msg.AdminID + "<@ADMINID>" + msg.MembersID + "<@MEMBERSID>" + msg.MembersProfileName + "<@MEMBERSPROFILENAME>" + msg.Timestamp + "<@TIMESTAMP>" + "<@EOF>";
                        UB.LogFile.Append(@"D:\Logs\", "Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
            }
            catch (Exception ex)
            {
                UB.LogFile.Append(@"D:\Logs\", ex.Message.ToString() + " " + ex.ToString());
            }
        }
    }
}
