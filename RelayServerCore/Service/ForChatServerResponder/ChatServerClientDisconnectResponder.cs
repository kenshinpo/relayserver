﻿using System;
using System.Linq;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.LoadBalancing;

namespace RelayServerCore.Service.ForChatServerResponder
{
    // Establish connection between Client-ChatServer and RelayServer
    public class ChatServerClientDisconnectResponder : MessageResponder
    {
        public ChatServerClientDisconnectResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.CLIENT_CHATSERVER_DISCONNECT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                UB.LogFile.Append(@"D:\Logs\","Client-ChatServer disconnect: " + message);
                ClientChatServerDisconnectMessage msg = RealizeMessage<ClientChatServerDisconnectMessage>(message);

                if (CherryChatUserManager.RemoveUserByCuid(msg.ChatUserID))
                {
                     // UB.LogFile.Append(@"D:\Logs\","Remove [" + msg.ChatUserID + "] success.");
                }
                else
                {
                     // UB.LogFile.Append(@"D:\Logs\","Remove [" + msg.ChatUserID + "] fail.");
                }
            }
            catch (Exception ex)
            {
                 UB.LogFile.Append(@"D:\Logs\",ex.ToString());
            }
        }
    }
}
