﻿using System;
using System.Linq;
using System.Text;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace RelayServerCore.Service.ForChatServerResponder
{
    public class ChatServerSearchUserResponderForGiftSent : MessageResponder
    {
        public ChatServerSearchUserResponderForGiftSent()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_SEARCHUSERFORGIFTSENT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                 // UB.LogFile.Append(@"D:\Logs\","Message for gift sent: " + message);

                ChatServerSearchUserMessageForGiftSent msg = RealizeMessage<ChatServerSearchUserMessageForGiftSent>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(msg.ChatUserID);
                if (ccuser != null)
                {
                     // UB.LogFile.Append(@"D:\Logs\",msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    // Ask intended chat server to send message
                    ServerSession chatServerToBeDispatched = onlineChatServers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@SENTGIFT>" + msg.ChatID + "<@CHATID>" + msg.MsgID + "<@MSGID>" + "<@EOF>";
                         // UB.LogFile.Append(@"D:\Logs\","Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
            }
            catch (Exception ex)
            {
                 // UB.LogFile.Append(@"D:\Logs\",ex.ToString());
            }
        }
    }
}
