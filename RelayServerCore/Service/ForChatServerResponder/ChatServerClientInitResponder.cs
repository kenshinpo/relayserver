﻿using System;
using System.Linq;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;

namespace RelayServerCore.Service.ForChatServerResponder
{
    // Establish connection between Client-ChatServer and RelayServer
    public class ChatServerClientInitResponder : MessageResponder
    {
        public ChatServerClientInitResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.CLIENT_CHATSERVER_INIT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                UB.LogFile.Append(@"D:\Logs\", "Client-ChatServer init: " + message);
                ClientChatServerInitMessage msg = RealizeMessage<ClientChatServerInitMessage>(message);

                CherryChatUser ccuser = new CherryChatUser { cuid = msg.ChatUserID, chatServerHostname = currentServer.ipAddress };
                if (CherryChatUserManager.AddUser(ccuser))
                {
                    // UB.LogFile.Append(@"D:\Logs\", String.Format("User {0} was saved to memcache", msg.ChatUserID));
                }
                else
                {
                    // UB.LogFile.Append(@"D:\Logs\", String.Format("User {0} was not saved to memcache", msg.ChatUserID));
                }
            }
            catch (Exception ex)
            {
                UB.LogFile.Append(@"D:\Logs\", ex.ToString());
            }
        }
    }
}
