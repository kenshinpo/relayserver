﻿using System;
using System.Linq;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.LoadBalancing;

namespace RelayServerCore.Service.ForChatServerResponder
{
    // Establish connection between ChatServer and RelayServer
    public class ChatServerInitResponder : MessageResponder
    {
        public ChatServerInitResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_INIT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                UB.LogFile.Append(@"D:\Logs\","ChatServer init: " + message);
                ChatServerInitMessage msg = RealizeMessage<ChatServerInitMessage>(message);

                currentServer.ipAddress = msg.ipAddress;

                // Create if not exist
                var existingChatServer = onlineChatServers[msg.ipAddress];

                if (existingChatServer == null)
                {
                    onlineChatServers[msg.ipAddress] = currentServer;
                    /* write to memcached */
                    ServerManager.InitChatServer(currentServer.ipAddress);
                }

                ChatServerInitAckMessage ack = new ChatServerInitAckMessage() { status = "true" };
                ReplyMessage(currentServer, ack);
            }
            catch (Exception ex)
            {
                 UB.LogFile.Append(@"D:\Logs\",ex.ToString());
            }
        }
    }
}
