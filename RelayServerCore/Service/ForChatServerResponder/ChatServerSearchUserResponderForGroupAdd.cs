﻿using System;
using System.Linq;
using System.Text;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.LoadBalancing;
using RelayServerCore.Data.Entity;

namespace RelayServerCore.Service.ForChatServerResponder
{
    public class ChatServerSearchUserResponderForGroupAdd : MessageResponder
    {
        public ChatServerSearchUserResponderForGroupAdd()
            : base()
        {
            MessageOfInterested = MessageFormat.CHATSERVER_SEARCHUSERFORGROUPADD.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                 // UB.LogFile.Append(@"D:\Logs\","Message for group pic update: " + message);

                ChatServerSearchUserMessageForGroupAdd msg = RealizeMessage<ChatServerSearchUserMessageForGroupAdd>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                     // UB.LogFile.Append(@"D:\Logs\",msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    // Ask intended chat server to send message
                    ServerSession chatServerToBeDispatched = onlineChatServers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@GROUPADDSYNC>" + msg.ChatID + "<@CHATID>"  + msg.MembersID + "<@MEMBERSID>" + msg.MembersProfileName + "<@MEMBERSPROFILENAME>" + msg.NotificationID + "<@NOTIFICATIONID>" + msg.NotificationMessage + "<@NOTIFICATIONMESSAGE>" + msg.Timestamp + "<@TIMESTAMP>" + "<@EOF>";
                         // UB.LogFile.Append(@"D:\Logs\","Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
               
            }
            catch (Exception ex)
            {
                 UB.LogFile.Append(@"D:\Logs\", ex.Message.ToString() + " " + ex.ToString());
            }
        }
    }
}
