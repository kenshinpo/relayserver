﻿using System;
using System.Linq;
using System.Text;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing;
using CherryChat.Connectivity.PushNotification;
using CherryChat.Message;

namespace RelayServerCore.Service.ForWebServiceResponder
{
    public class WSSearchUserResponderForLogoutCherryAccount : MessageResponder
    {
        public WSSearchUserResponderForLogoutCherryAccount()
            : base()
        {
            MessageOfInterested = MessageFormat.WS_SEARCHUSERFORLOGOUTCHERRYACCOUNT.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                // UB.LogFile.Append(@"D:\Logs\","Message for logout cherry account: " + message);

                WSSearchUserMessageForLogoutCherryAccount msg = RealizeMessage<WSSearchUserMessageForLogoutCherryAccount>(message);
                string chatUserIDToBeSearched = msg.ExistingChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                    // UB.LogFile.Append(@"D:\Logs\",msg.ExistingChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@LOGOUTCHERRYACCOUNT>" + msg.ExistingChatUserID + "<@EXISTINGCHATUSERID>" + "<@EOF>";
                        UB.LogFile.Append(@"D:\Logs\", "Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
                else
                {
                    // UB.LogFile.Append(@"D:\Logs\",msg.ExistingChatUserID + " is not at Memcache server. (OFFLINE)");

                    // Send notification 
                    // UB.LogFile.Append(@"D:\Logs\","Push notification");

                    if (msg.NotificationMessage.Length > 100) msg.NotificationMessage = msg.NotificationMessage.Substring(0, 97) + "...";
                    APNSManager.getInstance().SendNotification(msg.DeviceToken, MessageConverter.HtmlDecode(msg.NotificationMessage), "0", 0, msg.OperatingType);
                }
            }
            catch (Exception ex)
            {
                UB.LogFile.Append(@"D:\Logs\", ex.ToString());
            }
        }
    }
}
