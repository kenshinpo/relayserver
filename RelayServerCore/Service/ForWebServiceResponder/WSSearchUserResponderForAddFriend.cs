﻿using System;
using System.Linq;
using System.Text;
using RelayServerCore.Message;
using RelayServerCore.Message.ChatServer;
using RelayServerCore.LoadBalancing;
using RelayServerCore.Data.Entity;

namespace RelayServerCore.Service.ForWebServiceResponder
{
    public class WSSearchUserResponderForAddFriend : MessageResponder
    {
        public WSSearchUserResponderForAddFriend()
            : base()
        {
            MessageOfInterested = MessageFormat.WS_SEARCHUSERFORADDFRIEND.First();
        }

        public override void Respond(ServerSessionCollection onlineChatServers, ServerSession currentServer, string message)
        {
            try
            {
                 // UB.LogFile.Append(@"D:\Logs\","Message for add friend: " + message);

                WSSearchUserMessageForAddFriend msg = RealizeMessage<WSSearchUserMessageForAddFriend>(message);
                string chatUserIDToBeSearched = msg.ChatUserID;

                CherryChatUser ccuser = CherryChatUserManager.GetUser(chatUserIDToBeSearched);
                if (ccuser != null)
                {
                     // UB.LogFile.Append(@"D:\Logs\",msg.ChatUserID + "at [" + ccuser.chatServerHostname + "] ChatServer. (ONLINE)");

                    ServerSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary RS Service"].OnlineChatServers;

                    ServerSession chatServerToBeDispatched = onlineUsers[ccuser.chatServerHostname];

                    if (chatServerToBeDispatched != null)
                    {
                        string messageToBeForwarded = chatUserIDToBeSearched + "<@ADDFRIEND>" + msg.FriendChatUserID + "<@FRIENDCHATUSERID>" + msg.ChatUserID + "<@CHATUSERID>" + msg.ProfileName + "<@PROFILENAME>" +
                            msg.ProfileImageURL + "<@PROFILEIMAGEURL>" + msg.Status + "<@STATUS>" + msg.LastUpdatedProfileDate + "<@LASTUPDATEDPROFILEDATE>" + msg.RelationshipType + "<@RELATIONSHIPTYPE>" + "<@EOF>";
                         // UB.LogFile.Append(@"D:\Logs\","Message to be sent to ChatServer " + chatServerToBeDispatched.ipAddress + ", " + messageToBeForwarded);

                        chatServerToBeDispatched.Transport.Write(Encoding.UTF8.GetBytes(messageToBeForwarded));
                    }
                }
               
            }
            catch (Exception ex)
            {
                 UB.LogFile.Append(@"D:\Logs\",ex.ToString());
            }
        }
    }
}
