﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RelayServerCore.Connectivity;
using RelayServerCore.LoadBalancing;

namespace RelayServerCore.Service
{
    public class MessageService : IProtocol
    {
        private static char[] tagDelimiter = new char[] { '>' };

        protected ServerSessionCollection onlineChatServers;
        protected ServerSession currentServer;
        protected StringBuilder stringBuffer;
        protected Dictionary<string, MessageResponder> responders;

        public MessageService(ServerSessionCollection onlineChatServers, Dictionary<string, MessageResponder> responders)
        {
            this.onlineChatServers = onlineChatServers;
            this.stringBuffer = new StringBuilder();
            this.responders = responders;
            this.currentServer = new ServerSession();
        }

        public void ConnectionLost(ITransport transport)
        {
            UB.LogFile.Append(@"D:\Logs\", String.Format("ChatServer {0} disconnected from RS", currentServer.ipAddress));
            ServerManager.RemoveOnlineUserByServerHostname(currentServer.ipAddress);
            onlineChatServers.Remove(currentServer.ipAddress);
        }

        public void ConnectionMade(ITransport transport)
        {
            UB.LogFile.Append(@"D:\Logs\", String.Format("ChatServer {0} connected to RS", currentServer.ipAddress));
            currentServer.Transport = transport;
            transport.Read();
        }

        public void ConnectionTimeOut(ITransport transport)
        {
            currentServer.Transport.Close();
        }

        public void DataSent(byte[] data, int sentByteCount)
        {
            string sentStr = Encoding.UTF8.GetString(data);
            if (!sentStr.StartsWith("<@OK>"))
            {
                //UB.LogFile.Append(@"D:\Logs\", String.Format("{0} was sent to ChatServer: {1}.", sentStr, currentServer.ipAddress));
            }
        }

        public void DataReceived(byte[] data, int receivedByteCount)
        {
            stringBuffer.Append(Encoding.UTF8.GetString(data, 0, receivedByteCount));
            IEnumerable<string> msgs = ExtractMessages(stringBuffer.ToString());

            try
            {
                foreach (var msg in msgs)
                {
                    if (!msg.StartsWith("<@P>"))
                    {
                        //UB.LogFile.Append(@"D:\Logs\", String.Format("{0} was received from User {1}.", msg, currentServer.ipAddress));
                    }
                    int delimiterIndex = msg.IndexOf('>');
                    string tag = msg.Substring(0, delimiterIndex + 1);

                    //UB.LogFile.Append(@"D:\Logs\", String.Format("Receive message: \"{0}\" from ChatServer {1}.", msg, currentServer.ipAddress));

                    if (responders.ContainsKey(tag))
                    {
                        responders[tag].Respond(onlineChatServers, currentServer, msg);
                    }
                    else
                    {
                        UB.LogFile.Append(@"D:\Logs\", String.Format("Receive unknown message: \"{0}\" from ChatServer {1}.", msg, currentServer.ipAddress));
                    }
                        
                }

                if (currentServer.ReceiveNextMessage)
                    currentServer.Transport.Read();
            }
            catch (NullReferenceException ex)
            {
                UB.LogFile.Append(@"D:\Logs\", String.Format("Null reference exception occurs on Server {0} session, ignored.", currentServer.ipAddress));
            }
            catch (Exception ex)
            {
                UB.LogFile.Append(@"D:\Logs\", String.Format("Error occurs on ChetServer {0} session: {1} \nCall stack:\n {2}", currentServer.ipAddress, ex.Message, ex.StackTrace));
                currentServer.Transport.Close();
            }
        }

        private IEnumerable<string> ExtractMessages(string received)
        {
            List<string> msgList = new List<string>();
            int index = -1;

            while ((index = received.IndexOf("<@EOF>")) != -1)
            {
                stringBuffer.Remove(0, index + 6);
                msgList.Add(received.Substring(0, index + 6));
                received = received.Substring(index + 6);
            }
            return msgList;
        }

        private IEnumerable<string> TokenizeMessage(string msg)
        {
            List<string> strList = new List<string>(5);
            string head = string.Empty;
            string tail = msg;
            char[] seperator1 = { '>' };
            string[] seperator2 = { "<@" };

            while (tail != string.Empty)
            {
                string[] splited = tail.Split(seperator1, 2);
                string value = splited[0].Split(seperator2, 2, StringSplitOptions.None).First();
                if (value != "") strList.Add(value);
                tail = splited[1];
            }
            return strList;
        }
    }
}
