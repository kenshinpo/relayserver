﻿using RelayServerCore.Connectivity;

namespace RelayServerCore.Service
{

    /// <summary>
    /// Bundle that contains of user state.
    /// </summary>
    public class ServerSession
    {
        /// <summary>
        /// IP address of the server.
        /// </summary>
        public string ipAddress = string.Empty;

        /// <summary>
        /// The connection object which can be use to communicate with the server.
        /// </summary>
        public ITransport Transport = null;

        /// <summary>
        /// Indicate if server should automatically receive next message from this server.
        /// </summary>
        public bool ReceiveNextMessage = true;
    }
}
