﻿using System;
using System.Collections.Generic;
using RelayServerCore.Connectivity;
using CherryChat.Connectivity.PushNotification;

namespace RelayServerCore.Service
{
    public class MessageServiceFactory : IProtocolFactory
    {
        public static Dictionary<string, MessageServiceFactory> Instances = new Dictionary<string, MessageServiceFactory>();

        private Dictionary<string, MessageResponder> responders;

        public string Name { get; private set; }

        public ServerSessionCollection OnlineChatServers { get; private set; }

        public MessageServiceFactory(string name)
        {
            Name = name;
            responders = new Dictionary<string, MessageResponder>();
        }

        public void RegisterResponder(MessageResponder responder)
        {
            responders.Add(responder.MessageOfInterested, responder);
        }

        public void DropResponder(MessageResponder responder)
        {
            responders.Remove(responder.MessageOfInterested);
        }

        public void Start()
        {
            APNSManager.startManager(APNSManager.OPERATION_MODE.OPS_PRO_CC);
            OnlineChatServers = new ServerSessionCollection();

            if (Instances.ContainsKey(Name))
            {
                throw new InvalidOperationException("An instance with same name is already exists");
            }
            Instances[Name] = this;

            //UB.LogFile.Append(@"D:\Logs\", String.Format("{0} started.", Name));
        }

        public IProtocol CreateService()
        {
            return new MessageService(OnlineChatServers, responders);
        }

        public void Stop()
        {
            OnlineChatServers.ForEach(new Action<ServerSession>((cs) => cs.Transport.Close()));
            Instances.Remove(Name);
            UB.LogFile.Append(@"D:\Logs\", String.Format("{0} stopped.", Name));
        }

        public ServerSessionCollection GetOnlineChatServers()
        {
            return OnlineChatServers;
        }
    }
}
