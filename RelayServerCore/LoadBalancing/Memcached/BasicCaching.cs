﻿using System;
using Enyim.Caching;

namespace RelayServerCore.LoadBalancing.Memcached
{
    public class BasicCaching
    {
        public static String MEMCACHED_KEY_ONLINE_COUNT = "OnlineCount";

        public static MemcachedClient memcachedClient = new MemcachedClient("enyim.com/memcached");
    }
}   