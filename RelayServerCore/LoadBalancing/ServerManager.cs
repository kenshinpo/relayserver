﻿using System;
using System.Collections.Generic;
using System.Linq;
using Enyim.Caching.Memcached;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing.Memcached;

namespace RelayServerCore.LoadBalancing
{
    public class ServerManager : BasicCaching
    {
        private static readonly string DEFAULT_CHATSERVER_HOSTNAME = "ccs02.cherrycredits.com";

        public static string logPath = @"D:\Logs\";

        public static void InitChatServer(string serverHostname)
        {
            try
            {
                /* Step 1. Remove all data of serverName. */
                RemoveOnlineUserByServerHostname(serverHostname);

                /* Step 2. Add serverName count = 0 to OnlineCount */
                AddOnlineUserCount(serverHostname);

                /* Step 3. Add ChatServer to memcached */
                ChatServer chatServer = new ChatServer { hostname = serverHostname, onlineUsers = new List<string>() };
            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
            }
        }

        public static string GetLowestLoadServerHostname()
        {
            OnlineCounter currentServers = memcachedClient.Get<OnlineCounter>(MEMCACHED_KEY_ONLINE_COUNT);
            try
            {
                if (currentServers != null && currentServers.recorder != null && currentServers.recorder.Count > 0)
                    return currentServers.recorder.Keys.Cast<string>().Select(x => new { x, y = currentServers.recorder[x] }).OrderBy(x => x.y).First().x;
                else
                    return DEFAULT_CHATSERVER_HOSTNAME;
            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
                return DEFAULT_CHATSERVER_HOSTNAME;
            }
        }

        public static int GetOnlineUserCount(string serverHostname)
        {
            try
            {
                OnlineCounter currentServers = memcachedClient.Get<OnlineCounter>(MEMCACHED_KEY_ONLINE_COUNT);
                if (currentServers != null && currentServers.recorder != null && currentServers.recorder.ContainsKey(serverHostname))
                    return currentServers.recorder[serverHostname];
                else
                    return -1;
            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
                return -1;
            }
        }

        public static Dictionary<string, int> GetAllOnlineCount()
        {
            OnlineCounter currentServers = memcachedClient.Get<OnlineCounter>(MEMCACHED_KEY_ONLINE_COUNT);
            if (currentServers != null && currentServers.recorder != null)
                return currentServers.recorder;
            else
                return null;
        }

        public static void AddOnlineUserCount(string serverHostname)
        {
            try
            {
                OnlineCounter currentServers = memcachedClient.Get<OnlineCounter>(MEMCACHED_KEY_ONLINE_COUNT);
                if (currentServers != null)
                {
                    if (currentServers.recorder != null)
                    {
                        if (currentServers.recorder.ContainsKey(serverHostname))
                            currentServers.recorder[serverHostname]++;
                        else
                            currentServers.recorder.Add(serverHostname, 0);

                        OnlineCounter updateServers = new OnlineCounter { recorder = currentServers.recorder };
                        if (memcachedClient.Store(StoreMode.Replace, MEMCACHED_KEY_ONLINE_COUNT, updateServers))
                        {
                            // Replace OnlineCount to memcached success !
                            // UB.LogFile.Append(logPath, "Replace OnlineCount to memcached success !");
                        }
                        else
                        {
                            // Replace OnlineCount to memcached fail !
                            // UB.LogFile.Append(logPath, "Replace OnlineCount to memcached fail !");
                        }
                    }
                    else
                    {
                        Dictionary<string, int> recorderNew = new Dictionary<string, int>();
                        recorderNew.Add(serverHostname, 0);
                        OnlineCounter updateServers = new OnlineCounter { recorder = recorderNew };
                        if (memcachedClient.Store(StoreMode.Replace, MEMCACHED_KEY_ONLINE_COUNT, updateServers))
                        {
                            // Replace OnlineCount to memcached success !
                            // UB.LogFile.Append(logPath, "Replace OnlineCount to memcached success !");
                        }
                        else
                        {
                            // Replace OnlineCount to memcached fail !
                            // UB.LogFile.Append(logPath, "Replace OnlineCount to memcached fail !");
                        }
                    }
                }
                else
                {
                    Dictionary<string, int> recorderNew = new Dictionary<string, int>();
                    recorderNew.Add(serverHostname, 0);
                    OnlineCounter updateServers = new OnlineCounter { recorder = recorderNew };
                    if (memcachedClient.Store(StoreMode.Add, MEMCACHED_KEY_ONLINE_COUNT, updateServers))
                    {
                        // Add OnlineCount to memcached success !
                        // UB.LogFile.Append(logPath, "Add OnlineCount to memcached success !");
                    }
                    else
                    {
                        // Add OnlineCount to memcached fail !
                        // UB.LogFile.Append(logPath, "Add OnlineCount to memcached fail !");
                    }
                }
            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
            }
        }

        public static void SubOnlineUserCount(string serverHostname)
        {
            try
            {
                OnlineCounter currentServers = memcachedClient.Get<OnlineCounter>(MEMCACHED_KEY_ONLINE_COUNT);
                if (currentServers != null)
                {
                    if (currentServers.recorder != null)
                    {
                        if (currentServers.recorder.ContainsKey(serverHostname) && currentServers.recorder[serverHostname] > 0)
                            currentServers.recorder[serverHostname]--;
                        else
                            currentServers.recorder[serverHostname] = 0;

                        OnlineCounter serversUpdate = new OnlineCounter { recorder = currentServers.recorder };
                        bool success = memcachedClient.Store(StoreMode.Replace, MEMCACHED_KEY_ONLINE_COUNT, serversUpdate);
                        if (!memcachedClient.Store(StoreMode.Replace, MEMCACHED_KEY_ONLINE_COUNT, serversUpdate))
                        {
                            // Sub online user count fail !
                            // UB.LogFile.Append(logPath, "Sub online user count fail !");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
            }
        }

        public static void RemoveOnlineUserByServerHostname(string serverHostname)
        {
            try
            {
                /* Step 1. Remove CherryCharUser on memcached */
                ChatServer chatServer = memcachedClient.Get<ChatServer>(serverHostname);

                if (chatServer != null && chatServer.onlineUsers != null && chatServer.onlineUsers.Count > 0)
                {
                    for (int i = 0; i < chatServer.onlineUsers.Count; i++)
                    {
                        CherryChatUser ccuser = memcachedClient.Get<CherryChatUser>(chatServer.onlineUsers[i]);
                        if (ccuser != null)
                        {
                            if (memcachedClient.Remove(chatServer.onlineUsers[i]))
                            {
                                // Remove CherryChatUser on memcached success !
                                // UB.LogFile.Append(logPath, "Remove CherryChatUser on memcached success !");
                            }
                            else
                            {
                                // Remove CherryChatUser on memcached fail !
                                // UB.LogFile.Append(logPath, "Remove CherryChatUser on memcached fail !");
                            }
                        }
                    }
                }

                /* Step 2. Remove ChatServer on memcached */
                if (chatServer != null)
                {
                    if (memcachedClient.Remove(serverHostname))
                    {
                        // Remove ChatServer on memcached success !
                        // UB.LogFile.Append(logPath, "Remove ChatServer on memcached success !");
                    }
                    else
                    {
                        // Remove ChatServer on memcached fail !
                        // UB.LogFile.Append(logPath, "Remove ChatServer on memcached fail !");
                    }
                }

                /* Step 3. Remove ChatServer count on OnlineCount */
                OnlineCounter currentServers = memcachedClient.Get<OnlineCounter>(MEMCACHED_KEY_ONLINE_COUNT);

                if (currentServers != null && currentServers.recorder != null && currentServers.recorder.ContainsKey(serverHostname))
                {
                    Dictionary<string, int> updateRecorder = currentServers.recorder;
                    updateRecorder.Remove(serverHostname);
                    OnlineCounter updateServer = new OnlineCounter { recorder = updateRecorder };

                    if (memcachedClient.Store(StoreMode.Replace, MEMCACHED_KEY_ONLINE_COUNT, updateServer))
                    {
                        // Replace OnlineCount to memcached success !
                        // UB.LogFile.Append(logPath, "Replace OnlineCount to memcached success !");
                    }
                    else
                    {
                        // Replace OnlineCount to memcached fail !
                        // UB.LogFile.Append(logPath, "Replace OnlineCount to memcached fail !");
                    }
                }

            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
            }
        }

        public static void AddChatServer(ChatServer chatServer)
        {
            try
            {
                ChatServer currentChatServer = memcachedClient.Get<ChatServer>(chatServer.hostname);
                if (currentChatServer != null)
                {
                    if (memcachedClient.Store(StoreMode.Replace, chatServer.hostname, chatServer))
                    {
                        // Replace ChatServer to memcached success !
                        // UB.LogFile.Append(logPath, "Replace ChatServer to memcached success !");
                    }
                    else
                    {
                        // Replace ChatServer to memcached fail !
                        // UB.LogFile.Append(logPath, "Replace ChatServer to memcached fail !");
                    }
                }
                else
                {
                    if (memcachedClient.Store(StoreMode.Add, chatServer.hostname, chatServer))
                    {
                        // Add ChatServer to memcached success !
                        // UB.LogFile.Append(logPath, "Add ChatServer to memcached success !");
                    }
                    else
                    {
                        // Add ChatServer to memcached fail !
                        // UB.LogFile.Append(logPath, "Add ChatServer to memcached fail !");
                    }
                }
            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
            }
        }

        public static void RemoveChatServer(ChatServer chatServer)
        {
            try
            {
                ChatServer currentChatServer = memcachedClient.Get<ChatServer>(chatServer.hostname);
                if (currentChatServer != null)
                {
                    if (memcachedClient.Remove(chatServer.hostname))
                    {
                        // Remove ChatServer to memcached success !
                        // UB.LogFile.Append(logPath, "Remove ChatServer to memcached success !");
                    }
                    else
                    {
                        // Remove ChatServer to memcached fail !
                        // UB.LogFile.Append(logPath, "Remove ChatServer to memcached fail !");
                    }
                }
            }
            catch (Exception ex)
            {
                // exception
                UB.LogFile.Append(logPath, ex.ToString());
            }
        }
    }
}
