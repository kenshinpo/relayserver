﻿using System;
using System.Collections.Generic;
using Enyim.Caching.Memcached;
using RelayServerCore.Data.Entity;
using RelayServerCore.LoadBalancing.Memcached;

namespace RelayServerCore.LoadBalancing
{
    public class CherryChatUserManager : BasicCaching
    {
        public static string logPath = @"D:\Logs\";

        public static bool AddUser(CherryChatUser updateUser)
        {
            bool isAddUserSuccess = false;
            try
            {
                CherryChatUser currentUser = memcachedClient.Get<CherryChatUser>(updateUser.cuid);
                if (currentUser != null)
                {
                    if (memcachedClient.Store(StoreMode.Replace, updateUser.cuid, updateUser))
                    {
                        // Replace CherryChatUser to memcached success !

                        ServerManager.AddOnlineUserCount(updateUser.chatServerHostname);
                        ServerManager.SubOnlineUserCount(currentUser.chatServerHostname);

                        ChatServer currentChatServer = memcachedClient.Get<ChatServer>(updateUser.chatServerHostname);
                        if (currentChatServer != null)
                        {
                            List<string> updateOnlineUsers = currentChatServer.onlineUsers;
                            updateOnlineUsers.Remove(currentUser.cuid);
                            updateOnlineUsers.Add(updateUser.cuid);
                            ChatServer updateChatServer = new ChatServer { hostname = updateUser.chatServerHostname, onlineUsers = updateOnlineUsers };
                            ServerManager.AddChatServer(updateChatServer);
                        }
                        isAddUserSuccess = true;
                    }
                    else
                    {
                        // Replace CherryChatUser to memcached fail !
                    }
                }
                else
                {
                    if (memcachedClient.Store(StoreMode.Add, updateUser.cuid, updateUser))
                    {
                        // Add CherryChatUser to memcached success !

                        ServerManager.AddOnlineUserCount(updateUser.chatServerHostname);

                        ChatServer currentChatServer = memcachedClient.Get<ChatServer>(updateUser.chatServerHostname);
                        if (currentChatServer != null)
                        {
                            List<string> updateOnlineUser = currentChatServer.onlineUsers;
                            updateOnlineUser.Add(updateUser.cuid);
                            ChatServer updateChatServer = new ChatServer { hostname = currentChatServer.hostname, onlineUsers = updateOnlineUser };

                        }
                        else
                        {
                            List<string> updateOnlineUser = new List<string>();
                            updateOnlineUser.Add(updateUser.cuid);
                            ChatServer updateChatServer = new ChatServer { hostname = updateUser.chatServerHostname, onlineUsers = updateOnlineUser };
                        }

                        isAddUserSuccess = true;
                    }
                    else
                    {
                        // Add CherryChatUser to memcached fail !
                    }
                }
            }
            catch (Exception ex)
            {
                // exception
                isAddUserSuccess = false;
            }
            return isAddUserSuccess;
        }

        public static bool RemoveUserByCuid(string cuid)
        {
            bool isRemoveUserSuccess = false;
            try
            {
                CherryChatUser ccuser = memcachedClient.Get<CherryChatUser>(cuid);

                if (ccuser != null)
                {
                    if (memcachedClient.Remove(ccuser.cuid))
                    {
                        ServerManager.SubOnlineUserCount(ccuser.chatServerHostname);

                        ChatServer currentChatServer = memcachedClient.Get<ChatServer>(ccuser.chatServerHostname);

                        if (currentChatServer != null)
                        {
                            currentChatServer.onlineUsers.Remove(cuid);
                            if (memcachedClient.Store(StoreMode.Replace, currentChatServer.hostname, currentChatServer))
                            {
                                // Replace ChatServer to memcached success !
                            }
                            else
                            {
                                // Replace ChatServer to memcached fail !
                            }
                        }
                        else
                        {

                        }

                        isRemoveUserSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // exception
                isRemoveUserSuccess = false;
            }
            return isRemoveUserSuccess;
        }


        public static CherryChatUser GetUser(string cuid)
        {
            CherryChatUser ccuser = null;
            try
            {
                if (memcachedClient.Get<CherryChatUser>(cuid) != null)
                    ccuser = memcachedClient.Get<CherryChatUser>(cuid);
            }
            catch (Exception ex)
            {
                // exception
                ccuser = null;
            }
            return ccuser;
        }
    }
}
