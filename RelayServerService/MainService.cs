﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using TestLoadBalancer;

namespace RelayServerService
{
    partial class MainService : ServiceBase
    {
        LoadBalancerLauncher lbl;

        public MainService()
        {
            InitializeComponent();
            lbl = new LoadBalancerLauncher();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                lbl.Start(); 
            }
            catch (Exception ex)
            {
                
            }
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.

            try
            {
                lbl.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
